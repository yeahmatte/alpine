package gpxparser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import model.activity.Activity;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;
import model.gpsanalyzer.GpsLogAnalyzer;
import model.gpsanalyzer.GpsPoint;
import model.gpsanalyzer.utils.GpxReader;
import model.gpsanalyzer.utils.topografix.gpx._1._1.GpxType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.TrkType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.TrksegType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.WptType;
import resource.activity.ActivityGenerator;
import resource.activity.ActivityInterface;
import resource.activity.ActivityTypeGenerator;
import resource.activity.GpsPointInterface;

public class GpsAnalyzerTest {

	public static void main(String[] args) {
		InputStream stream = null;
		PrintWriter writer = null;

		try {
			stream = new FileInputStream("C:\\Users\\yeahmatte\\Documents\\git\\alpine\\GPXTestFiles\\activity_giro.gpx");
			writer = new PrintWriter("the-file-name.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		
		List<GpsPointInterface> gpsPoints = new ArrayList<GpsPointInterface>();
		
		//Parses the file to java object
		GpxReader gpxReader = new GpxReader();
		GpxType gpxObject = gpxReader.readGpx(stream);

		List<TrkType> gpxTrk = gpxObject.getTrk();
		TrkType gpxTrk1 = (gpxTrk.size() > 0) ? gpxTrk.get(0) : null;
		List<TrksegType> trkseg = (gpxTrk1 != null) ? gpxTrk1.getTrkseg() : null;
		TrksegType gpxTrkseg1 = (trkseg != null && trkseg.size() > 0) ? trkseg.get(0) : null;
		List<WptType> gpxTrkSeg1WPTs = gpxTrkseg1.getTrkpt();

		//Converts waypoints to GpsPoints
		for(WptType wpt : gpxTrkSeg1WPTs){
			gpsPoints.add(new GpsPoint(wpt.getLon().doubleValue(), wpt.getLat().doubleValue(),
					wpt.getEle().doubleValue(),wpt.getTime().toGregorianCalendar()));
		}

		ActivityInterface activity = new Activity();
		
		String activityName = gpxTrk1.getName();
		if(activityName == null || activityName.compareTo("")==0)
			activityName = "Hej";
		
		//Set basic values
		activity.setUserId(1);
		activity.setName( activityName );
		activity.setDescription(gpxTrk1.getDesc());
		activity.setActivityType(ActivityTypeGenerator.getInstance().getObjectFromString("Skiing"));
		activity.setStartCoords(gpsPoints.get(0).getLatitude(), gpsPoints.get(0).getLongitude());
		activity.setStartTime(gpsPoints.get(0).getTime());
		activity.setGpsPoints(gpsPoints);

		// Add total time and distance
		GpsLogAnalyzer logAnalyzer = new GpsLogAnalyzer();
		logAnalyzer.setGpsPoints(activity.getGpsPoints());
		logAnalyzer.setActivityType(activity.getActivityType());
		logAnalyzer.analyze();
		logAnalyzer.setValuesToActivity(activity);
		
		List<AbstractGpsAnalyzerContainer> containers = activity.getActivityType().getAnalyzerContainers();
		for ( AbstractGpsAnalyzerContainer c : containers) {
			System.out.println(c.toString());
		}
		
		ActivityInterface activityFromDb = ActivityGenerator.getInstance().getActivity(1, true);
		
		GpsLogAnalyzer logAnalyzerDb = new GpsLogAnalyzer();
		logAnalyzerDb.setGpsPoints(activityFromDb.getGpsPoints());
		logAnalyzerDb.setActivityType(activityFromDb.getActivityType());
		logAnalyzerDb.analyze();
		
		List<AbstractGpsAnalyzerContainer> containersDb = activityFromDb.getActivityType().getAnalyzerContainers();
		for ( AbstractGpsAnalyzerContainer c : containersDb) {
			System.out.println(c.toString());
		}
		

	}

}
