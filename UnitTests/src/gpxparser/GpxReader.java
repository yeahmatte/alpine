package gpxparser;

import com.topografix.gpx._1._0.Gpx;
import com.topografix.gpx._1._1.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class GpxReader {

	private static void printWpt(PrintWriter writer, WptType gpxWpt1){
		writer.println(" wptEle = "+gpxWpt1.getEle() +" )");
		writer.println(" wptTime = "+ gpxWpt1.getTime().getMillisecond()+" )");
		writer.println(" wptMagvar = "+gpxWpt1.getMagvar()+" )");
		writer.println(" wptGeoidheight  = "+gpxWpt1.getGeoidheight()+" )");
		writer.println(" wptName  = "+gpxWpt1.getName()+" )");
		writer.println(" wptCmt  = "+gpxWpt1.getCmt()+" )");
		writer.println(" wptDesc = "+gpxWpt1.getDesc()+" )");
		writer.println(" wptSrc  = "+gpxWpt1.getSrc()+" )");
		//List<LinkType> wpt1Link  = gpxWpt1.getLink();
		writer.println(" wptSym  = "+gpxWpt1.getSym()+" )");
		writer.println(" wptType  = "+gpxWpt1.getType()+" )");
		writer.println(" wptSat  = "+gpxWpt1.getSat()+" )");
		writer.println(" wptHdop  = "+gpxWpt1.getHdop()+" )");
		writer.println(" wptVdop = "+gpxWpt1.getVdop()+" )");
		writer.println(" wptPdop = "+gpxWpt1.getPdop()+" )");
		writer.println(" wptAgeofdgpsdata = "+gpxWpt1.getAgeofdgpsdata()+" )");
		writer.println(" wptDgpsid = "+gpxWpt1.getDgpsid()+" )");
		//ExtensionsType wpt1Extensions = gpxWpt1.getExtensions();
		writer.println(" wptLat = "+gpxWpt1.getLat()+" )");
		writer.println(" wptLon = "+gpxWpt1.getLon()+" )");
	}

	public static void main(String[] args) {

		GpxReader reader = new GpxReader();
		InputStream stream = null;
		PrintWriter writer = null;

		try {
			stream = new FileInputStream("C:\\Users\\yeahmatte\\Documents\\git\\alpine\\GPXTestFiles\\activity_giro.gpx");
			writer = new PrintWriter("the-file-name.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		GpxType gpx = reader.readGpx(stream);

		//Values in gpx
		String gpxVersion = gpx.getVersion();
		String gpxCreator = gpx.getCreator();

		MetadataType gpxMeta = gpx.getMetadata();
		List<WptType> gpxWpt = gpx.getWpt();
		List<RteType> gpxRte = gpx.getRte();
		List<TrkType> gpxTrk = gpx.getTrk();
		ExtensionsType gpxExtensions = gpx.getExtensions();



		writer.println("===== GpxType ====");
		writer.println("Gpx Version: "+gpxVersion);
		writer.println("Gpx Creator: "+gpxCreator);



		//Meta
		writer.println("\n\n==== MetaType in gpx =====\n");

		String 				metaName 		= gpxMeta.getName();
		String 				metaDesc 		= gpxMeta.getDesc();
		String 				metaKeywords 	= gpxMeta.getKeywords();

		PersonType 			metaAuthor 		= gpxMeta.getAuthor();
		CopyrightType 		metaCopyright 	= gpxMeta.getCopyright();
		//List<LinkType> 		metaLink 		= gpxMeta.getLink();
		XMLGregorianCalendar metaTime 		= gpxMeta.getTime();
		BoundsType 			metaBounds 		= gpxMeta.getBounds();
		ExtensionsType 		metaExtensions 	= gpxMeta.getExtensions();

		writer.println("metaName: "+metaName);
		writer.println("metaDesc: "+metaDesc);
		writer.println("metaDesc: "+metaKeywords);

		//writer.println("metaPerson( name:"+metaAuthor.getName()+" email: "+metaAuthor.getEmail()+")"); //Nullpointer
		//writer.println("metaCopyright( name: "+metaCopyright.getAuthor()+" )");	//Nullpointer
		writer.println("metaTime ( milliseconds: "+metaTime.getMillisecond()+" )");
		//writer.println("metaBounds (minLat: "+metaBounds.getMinlat()+" maxLat: "+metaBounds.getMaxlat()+" )"); //Nullpointer
		//writer.println("metaExtensions ( "+metaExtensions.getAny().get(0).toString()+" )"); //Nullpointer

		writer.println("\n\n==== gpxWpt1 ===== \n");
		writer.println("gpxWpt1.size: "+gpxWpt.size());
		//Waypoints gpxWpt
		WptType gpxWpt1 = (gpxWpt.size() > 0)? gpxWpt.get(0):null;
		if(gpxWpt1 != null) {

			printWpt(writer,gpxWpt1);
			/*
			BigDecimal wpt1Ele = gpxWpt1.getEle();
			XMLGregorianCalendar wpt1Time = gpxWpt1.getTime();
			BigDecimal wpt1Magvar = gpxWpt1.getMagvar();
			BigDecimal wpt1Geoidheight  = gpxWpt1.getGeoidheight();
			String wpt1Name  = gpxWpt1.getName();
			String wpt1Cmt  = gpxWpt1.getCmt();
			String wpt1Desc = gpxWpt1.getDesc();
			String wpt1Src  = gpxWpt1.getSrc();
			//List<LinkType> wpt1Link  = gpxWpt1.getLink();
			String wpt1Sym  = gpxWpt1.getSym();
			String wpt1Type  = gpxWpt1.getType();
			BigInteger wpt1Sat  = gpxWpt1.getSat();
			BigDecimal wpt1Hdop  = gpxWpt1.getHdop();
			BigDecimal wpt1Vdop = gpxWpt1.getVdop();
			BigDecimal wpt1Pdop = gpxWpt1.getPdop();
			BigDecimal wpt1Ageofdgpsdata = gpxWpt1.getAgeofdgpsdata();
			Integer wpt1Dgpsid = gpxWpt1.getDgpsid();
			ExtensionsType wpt1Extensions = gpxWpt1.getExtensions();
			BigDecimal wpt1Lat = gpxWpt1.getLat();
			BigDecimal wpt1Lon = gpxWpt1.getLon();
			 */
		}

		writer.println("\n\n==== gpxRte ===== \n");
		writer.println("---- Rte .get(0) ---\n");
		writer.println("gpxRte.size: "+gpxRte.size());
		//gpxRte
		RteType gpxRte1 = (gpxRte.size()>0)? gpxRte.get(0): null;
		if(gpxRte1 != null) {
			String rteName = gpxRte1.getName();
			String rteCmt = gpxRte1.getCmt();
			String rteDesc = gpxRte1.getDesc();
			String rteSrc = gpxRte1.getSrc();
			//List<LinkType> link;
			BigInteger rteNumber = gpxRte1.getNumber();
			String rteType = gpxRte1.getType();
			//ExtensionsType rteExtensions;
			List<WptType> rteRtept = gpxRte1.getRtept();

			writer.println("rteName: "+rteName);
			writer.println("rteCmt: "+rteCmt);
			writer.println("rteDesc: "+rteDesc);
			writer.println("rteSrc: "+rteSrc);
			writer.println("rteNumber: "+rteNumber);
			writer.println("rteType: "+rteType);

			WptType wptPoint =(rteRtept.size()>0)? rteRtept.get(0):null;
			if(wptPoint != null){
				writer.println("\n ---- rteRtept.get(0) ------\n");
				printWpt(writer,wptPoint);
			}

		}



		//-----------Trk------------------------
		writer.println("\n\n==== gpxTrk ===== \n");
		writer.println("---- Trk .get(0) ---\n");
		writer.println("gpxTrk.size: "+gpxTrk.size());

		TrkType gpxTrk1 = (gpxTrk.size() > 0)? gpxTrk.get(0):null;

		if(gpxTrk1 != null) {
			String trkName = gpxTrk1.getName();
			String trkCmt = gpxTrk1.getCmt();
			String trkDesc = gpxTrk1.getDesc();
			String trkSrc = gpxTrk1.getSrc();
			//List<LinkType> link;
			BigInteger trkNumber = gpxTrk1.getNumber();
			String trkType =  gpxTrk1.getType();
			//ExtensionsType extensions ;

			writer.println("trkName: "+trkName);
			writer.println("trkCmt: "+trkCmt);
			writer.println("trkDesc: "+trkDesc);
			writer.println("trkSrc: "+trkSrc);
			writer.println("trkNumber: "+trkNumber);
			writer.println("trkType: "+trkType);

			List<TrksegType> trkseg = gpxTrk1.getTrkseg();
			writer.println("trkseg.size: "+trkseg.size());

			TrksegType gpxTrkseg1 = (trkseg.size() > 0)? trkseg.get(0):null;

			if(gpxTrkseg1 != null){
				writer.println("----gpxTrkseg1---");
			    List<WptType> gpxTrkSeg1WPTS = gpxTrkseg1.getTrkpt();
			    ExtensionsType extensions = gpxTrkseg1.getExtensions();
			    
			    writer.println("gpxTrkSegWPTS.size: "+gpxTrkSeg1WPTS.size());
			    WptType segWpt1 = (gpxTrkSeg1WPTS.size()>0)?gpxTrkSeg1WPTS.get(0):null;
			    if(segWpt1 != null)
			    	printWpt(writer,segWpt1);
			    
			}


		}


		writer.close();
	}

	public GpxReader() {

	}

	public GpxType readGpx(InputStream gpxInputStream) {
		try{
			JAXBContext ctx = JAXBContext.newInstance(GpxType.class,Gpx.class);
			Unmarshaller um = ctx.createUnmarshaller();
			Object o = um.unmarshal(gpxInputStream);
			if(o instanceof Gpx){
				Gpx gpx = (Gpx)o;
				return convertFromOld(gpx);
			}
			else if(o instanceof JAXBElement){
				JAXBElement<GpxType> root = (JAXBElement<GpxType>)o;
				return root.getValue();
			}
			else {
				return null;
			}
		}
		catch (JAXBException e){
			System.out.println("e = " + e);
			return null;
		}
	}

	GpxType convertFromOld(Gpx gpx){
		GpxType newGpx = new GpxType();
		newGpx.setMetadata(createMetadata(gpx));
		newGpx.setCreator(gpx.getCreator());
		newGpx.setVersion(gpx.getVersion());


		List<WptType> wpts = newGpx.getWpt();
		wpts = createWaypoints(gpx.getWpt());

		List<RteType> rte = newGpx.getRte();
		createRoutes(gpx.getRte(),rte);

		List<TrkType> trk = newGpx.getTrk();
		createTracks(gpx.getTrk(),trk);

		return newGpx;
	}

	List<TrkType> createTracks(List<Gpx.Trk> trks,List<TrkType> tracks){

		for(Gpx.Trk trk:trks){
			System.out.println("trk = " + trk);
			tracks.add(createTracks(trk));
		}
		return tracks;
	}

	TrkType createTracks(Gpx.Trk trk){
		TrkType track = new TrkType();
		track.setCmt(trk.getCmt());
		track.setDesc(trk.getDesc());
		track.setName(trk.getName());
		track.setNumber(trk.getNumber());
		track.setSrc(trk.getSrc());
		List<TrksegType> trksegs = track.getTrkseg();
		for(Gpx.Trk.Trkseg trkseg:trk.getTrkseg()){
			trksegs.add(createTrackSeg(trkseg));

		}

		return track;
	}

	TrksegType createTrackSeg(Gpx.Trk.Trkseg trkseg){
		TrksegType newTrkseg = new TrksegType();

		List<WptType> trkpts = newTrkseg.getTrkpt();
		for(Gpx.Trk.Trkseg.Trkpt trkpt:trkseg.getTrkpt()){
			trkpts.add(convertOldTrkPntToWpt(trkpt));
		}

		return newTrkseg;

	}

	List<RteType> createRoutes(List<Gpx.Rte> routes,List<RteType> newRoutes){
		for(Gpx.Rte route:routes){
			newRoutes.add(createRoute(route));
		}
		return newRoutes;
	}

	RteType createRoute(Gpx.Rte route){
		RteType newRoute = new RteType();
		newRoute.setCmt(route.getCmt());
		newRoute.setDesc(route.getDesc());

		newRoute.setName(route.getName());
		newRoute.setNumber(route.getNumber());
		newRoute.setSrc(route.getSrc());

		List<WptType> wpts = newRoute.getRtept();
		List<Gpx.Rte.Rtept> rtpts = route.getRtept();
		for(Gpx.Rte.Rtept rtept:rtpts){
			wpts.add(convertOldRtePntToWpt(rtept));
		}

		return newRoute;
	}

	List<WptType> createWaypoints(List<Gpx.Wpt> wpts){
		List<WptType> newWpts = new ArrayList<WptType>();

		for(Gpx.Wpt wpt:wpts){
			newWpts.add(convertWaypoints(wpt));

		}
		return newWpts;
	}

	WptType convertOldTrkPntToWpt(Gpx.Trk.Trkseg.Trkpt trkpt){
		WptType newWpt = new WptType();

		newWpt.setAgeofdgpsdata(trkpt.getAgeofdgpsdata());
		newWpt.setCmt(trkpt.getCmt());
		newWpt.setDesc(trkpt.getDesc());
		newWpt.setDgpsid(trkpt.getDgpsid());
		newWpt.setEle(trkpt.getEle());
		newWpt.setFix(trkpt.getFix());
		newWpt.setGeoidheight(trkpt.getGeoidheight());
		newWpt.setHdop(trkpt.getGeoidheight());
		newWpt.setLat(trkpt.getLat());
		newWpt.setLon(trkpt.getLon());
		newWpt.setMagvar(trkpt.getMagvar());
		newWpt.setName(trkpt.getName());
		newWpt.setPdop(trkpt.getPdop());
		newWpt.setSat(trkpt.getSat());
		newWpt.setSrc(trkpt.getSrc());
		newWpt.setSym(trkpt.getSym());
		newWpt.setTime(trkpt.getTime());
		newWpt.setType(trkpt.getType());
		newWpt.setVdop(trkpt.getVdop());

		return newWpt;
	}
	WptType convertOldRtePntToWpt(Gpx.Rte.Rtept rtept){
		WptType newWpt = new WptType();

		newWpt.setAgeofdgpsdata(rtept.getAgeofdgpsdata());
		newWpt.setCmt(rtept.getCmt());
		newWpt.setDesc(rtept.getDesc());
		newWpt.setDgpsid(rtept.getDgpsid());
		newWpt.setEle(rtept.getEle());
		newWpt.setFix(rtept.getFix());
		newWpt.setGeoidheight(rtept.getGeoidheight());
		newWpt.setHdop(rtept.getGeoidheight());
		newWpt.setLat(rtept.getLat());
		newWpt.setLon(rtept.getLon());
		newWpt.setMagvar(rtept.getMagvar());
		newWpt.setName(rtept.getName());
		newWpt.setPdop(rtept.getPdop());
		newWpt.setSat(rtept.getSat());
		newWpt.setSrc(rtept.getSrc());
		newWpt.setSym(rtept.getSym());
		newWpt.setTime(rtept.getTime());
		newWpt.setType(rtept.getType());
		newWpt.setVdop(rtept.getVdop());

		return newWpt;
	}

	WptType convertWaypoints(Gpx.Wpt wpt){
		WptType newWpt = new WptType();

		newWpt.setAgeofdgpsdata(wpt.getAgeofdgpsdata());
		newWpt.setCmt(wpt.getCmt());
		newWpt.setDesc(wpt.getDesc());
		newWpt.setDgpsid(wpt.getDgpsid());
		newWpt.setEle(wpt.getEle());
		newWpt.setFix(wpt.getFix());
		newWpt.setGeoidheight(wpt.getGeoidheight());
		newWpt.setHdop(wpt.getGeoidheight());
		newWpt.setLat(wpt.getLat());
		newWpt.setLon(wpt.getLon());
		newWpt.setMagvar(wpt.getMagvar());
		newWpt.setName(wpt.getName());
		newWpt.setPdop(wpt.getPdop());
		newWpt.setSat(wpt.getSat());
		newWpt.setSrc(wpt.getSrc());
		newWpt.setSym(wpt.getSym());
		newWpt.setTime(wpt.getTime());
		newWpt.setType(wpt.getType());
		newWpt.setVdop(wpt.getVdop());

		return newWpt;

	}

	MetadataType createMetadata(Gpx gpx){
		MetadataType md = new MetadataType();
		md.setName(gpx.getName());
		md.setDesc(gpx.getDesc());
		md.setKeywords(gpx.getKeywords());
		md.setTime(gpx.getTime());
		md.setBounds(convertFrom10BoundsTo11Bounds(gpx.getBounds()));
		md.setAuthor(createAuthor(gpx.getAuthor(),gpx.getEmail(),gpx.getUrl(),gpx.getUrlname()));
		return md;
	}


	PersonType createAuthor(String name, String email, String url, String urlname){
		PersonType person = new PersonType();
		if(name!= null){
			person.setName(name);
		}

		if(url != null && urlname != null){
			LinkType link = new LinkType();
			link.setHref(url);
			link.setText(urlname);
			person.setLink(link);
		}

		EmailType emailType = new EmailType();
		if(email != null){
			String[] splitMail = email.split("@");
			emailType.setId(splitMail[0]);
			emailType.setDomain(splitMail[1]);
			person.setEmail(emailType);
		}
		return person;



	}

	BoundsType convertFrom10BoundsTo11Bounds(com.topografix.gpx._1._0.BoundsType bounds){
		if(bounds != null){
			BoundsType newBounds = new BoundsType();
			newBounds.setMaxlat(bounds.getMaxlat());
			newBounds.setMaxlon(bounds.getMaxlon());
			newBounds.setMinlat(bounds.getMinlat());
			newBounds.setMinlon(bounds.getMinlon());
			return newBounds;
		}
		else {
			return null;
		}
	}
}
