package UrlManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UrlFactory {

	public static String getURLToServlet(String servletName){
		for(Package packageName : Package.getPackages()) {
			try {
				//System.out.println(packageName.getName()+"."+servletName);
				Class c = Class.forName(packageName.getName()+"."+servletName);
				Method m = c.getDeclaredMethod("getURL", null);
				return (String) m.invoke(null, null);
			} catch( ClassNotFoundException e ) {
				//e.printStackTrace();
			} catch (IllegalAccessException e) {
				//e.printStackTrace();
			} catch (IllegalArgumentException e) {
				//e.printStackTrace();
			} catch (InvocationTargetException e) {
				//e.printStackTrace();
			} catch (NoSuchMethodException e) {
				//e.printStackTrace();
			} catch (SecurityException e) {
				//e.printStackTrace();
			} 
		}
		return "404";
	}
}
