package htmlParser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class TemplateFactory {
	private final static String templatePath = "";
	private final static Charset charset = StandardCharsets.UTF_8;
	
	private static String getFile(String fileName) throws IOException{
		return new String(Files.readAllBytes(Paths.get(templatePath+fileName)), charset);
	}
	
	private static String replacer(String baseString, Map<String,String> replacements){
		for(String key : replacements.keySet())
			baseString.replaceAll(key,replacements.get(key));
		return baseString;
	}
	
	public static String createPageHeader(String userAccount){
		//HTMLFactory.getLink(URLFactory.getURL("userProfile",userAccount.getUserId()),userAccount.getUserName());
		Map<String,String> parserValues = new HashMap<String,String>(); 
		
		parserValues.put("<:UserName:>",userAccount);
		parserValues.put("<:UserNameWithLink:>",userAccount);
		parserValues.put("<:MainLink:>",userAccount);
		parserValues.put("<:CurrentLink:>",userAccount);
		
		try {
			return  replacer(getFile("pageheader.html"),parserValues);
		} catch (IOException e) {
			return "Template not found";
		}
	}
	
}
