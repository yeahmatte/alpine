import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;


public class AsciiTable {

	public static void main(String[] args) {
		try{
			PrintWriter writer = new PrintWriter("asciiTable.txt", "UTF-8");
			for(char i=0;i<=256;i++){
				writer.printf("%d\t%s\t", Integer.valueOf(i), i);
				if((Integer.valueOf(i) % 4) == 3)
					writer.println("");
			}
			writer.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
