package junit.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import utils.PatternValidator;

/**
 * Tests the utils.PatternValidator class.
 * Each method in the class shall have one
 * test method in this test class.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public class PatternValidatorTests {
		
	
		/**
		 * Test the username pattern
		 */
	   @Test
	   public void testUserNamePatterns() {
		   String username1 = "matte";
		   String username2 = "matt3";
		   String username3 = " matte";
		   String username4 = "matte ";
		   String username5 = "m@tte";
		   String username6 = "matt_e";
		   String username7 = "";
		   String username8 = "m";
		   String username9 = "mat te";
		   String username10 = "matteaaaaaaaaaaaaaaaaaa";
		   		   
		   assertEquals(PatternValidator.checkUserName(username1),true);   
		   assertEquals(PatternValidator.checkUserName(username2),true);   
		   assertEquals(PatternValidator.checkUserName(username3),false);   
		   assertEquals(PatternValidator.checkUserName(username4),false);   
		   assertEquals(PatternValidator.checkUserName(username5),false);   
		   assertEquals(PatternValidator.checkUserName(username6),false);   
		   assertEquals(PatternValidator.checkUserName(username7),false);   
		   assertEquals(PatternValidator.checkUserName(username8),false);   
		   assertEquals(PatternValidator.checkUserName(username9),false);   
		   assertEquals(PatternValidator.checkUserName(username10),false);   
	   }
	   
	   /**
	    * Tests first name patterns
	    */
	   @Test
	   public void testFirstNamePatterns() {
		   String firstname1 = "Mattias";
		   String firstname2 = " Mattias";
		   String firstname3 = "Mattias ";
		   String firstname4 = "Mattia3s";
		   String firstname5 = "Mattias Muriel";
		   String firstname6 = "M";
		   String firstname7 = "";
		   String firstname8 = "mAtTiAs";
		   String firstname9 = "M@ttias";
		   String firstname10 = "Mattia$";
		   String firstname11 = "Mattiasaaaaaaaaaaaaaaaaaaaaaaaa";

		   assertEquals(PatternValidator.checkFirstName(firstname1),true);
		   assertEquals(PatternValidator.checkFirstName(firstname2),true);
		   assertEquals(PatternValidator.checkFirstName(firstname3),true);
		   assertEquals(PatternValidator.checkFirstName(firstname4),false);
		   assertEquals(PatternValidator.checkFirstName(firstname5),true);
		   assertEquals(PatternValidator.checkFirstName(firstname6),false);
		   assertEquals(PatternValidator.checkFirstName(firstname7),false);
		   assertEquals(PatternValidator.checkFirstName(firstname8),true);
		   assertEquals(PatternValidator.checkFirstName(firstname9),false);
		   assertEquals(PatternValidator.checkFirstName(firstname10),false);
		   assertEquals(PatternValidator.checkFirstName(firstname11),false); 
	   }
	   
	   /**
	    * tests surname patterns
	    */
	   @Test
	   public void testSurNamePatterns() {
		   String surname1 = "Mattias";
		   String surname2 = " Mattias";
		   String surname3 = "Mattias ";
		   String surname4 = "Mattia3s";
		   String surname5 = "Mattias Muriel";
		   String surname6 = "M";
		   String surname7 = "";
		   String surname8 = "mAtTiAs";
		   String surname9 = "M@ttias";
		   String surname10 = "Mattia$";
		   String surname11 = "Mattiasaaaaaaaaaaaaaaaaaaaaaabba";

		   assertEquals(PatternValidator.checkSurname(surname1),true);
		   assertEquals(PatternValidator.checkSurname(surname2),true);
		   assertEquals(PatternValidator.checkSurname(surname3),true);
		   assertEquals(PatternValidator.checkSurname(surname4),false);
		   assertEquals(PatternValidator.checkSurname(surname5),true);
		   assertEquals(PatternValidator.checkSurname(surname6),false);
		   assertEquals(PatternValidator.checkSurname(surname7),false);
		   assertEquals(PatternValidator.checkSurname(surname8),true);
		   assertEquals(PatternValidator.checkSurname(surname9),false);
		   assertEquals(PatternValidator.checkSurname(surname10),false);
		   assertEquals(PatternValidator.checkSurname(surname11),false); 
	   }
	   
	   /**
	    * tests email patterns
	    */
	   @Test
	   public void testEmailPatterns() {
		   String email1 = "mattias";
		   String email2 = "mattias@gmai.com";
		   String email3 = "mattias.m@gmail.com";
		   String email4 = "mattias!@gmail.com";
		   String email5 = "mattias:@gmail.com";
		   String email6 = "mattias@gmail:com";
		   String email7 = "mattias@gmail.sub.com";
		   String email8 = "mattias.m@gma.c.com";
		   String email9 = "m@tti@s.com";

		   assertEquals(PatternValidator.checkEmailAddress(email1),false);
		   assertEquals(PatternValidator.checkEmailAddress(email2),true);
		   assertEquals(PatternValidator.checkEmailAddress(email3),true);
		   assertEquals(PatternValidator.checkEmailAddress(email4),false);
		   assertEquals(PatternValidator.checkEmailAddress(email5),false);
		   assertEquals(PatternValidator.checkEmailAddress(email6),false);
		   assertEquals(PatternValidator.checkEmailAddress(email7),true);
		   assertEquals(PatternValidator.checkEmailAddress(email8),true);
		   assertEquals(PatternValidator.checkEmailAddress(email9),false);
	   }
	   
	   /**
	    * test password patterns
	    */
	   @Test
	   public void testPasswordPatterns() {
		   String password1 = "P@ASword";
		   String password2 = "P@ASword!!!!";
		   String password3 = "P@ASword :safa";
		   String password4 = "P@ASword                 ::::";
		   String password5 = " sASword";
		   String password6 = "P__ASword";
		   String password7 = "P@ASword^";
		   String password8 = "P@ASword\"";
		   String password9 = "P@ASword**";
		   
		   assertEquals(PatternValidator.checkPassword(password1),true);
		   assertEquals(PatternValidator.checkPassword(password2),true);
		   assertEquals(PatternValidator.checkPassword(password3),true);
		   assertEquals(PatternValidator.checkPassword(password4),true);
		   assertEquals(PatternValidator.checkPassword(password5),true);
		   assertEquals(PatternValidator.checkPassword(password6),true);
		   assertEquals(PatternValidator.checkPassword(password7),true);
		   assertEquals(PatternValidator.checkPassword(password8),false);
		   assertEquals(PatternValidator.checkPassword(password9),true);
	   }
}
