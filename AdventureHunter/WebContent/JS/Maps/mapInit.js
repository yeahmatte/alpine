	var map; //complex object of type OpenLayers.Map
		var defaultStyle = new OpenLayers.Style({
			'strokeWidth': 3,
			'strokeColor': '#ff0000'
		});

		var styleMap = new OpenLayers.StyleMap({'default': defaultStyle});
		
		function init() {
			map = new OpenLayers.Map ("map", {
				controls:[
					new OpenLayers.Control.Navigation(),
					new OpenLayers.Control.PanZoomBar(),
					new OpenLayers.Control.LayerSwitcher(),
					new OpenLayers.Control.Attribution()],
				maxExtent: new OpenLayers.Bounds(-20037508.34,-20037508.34,20037508.34,20037508.34),
				maxResolution: 156543.0399,
				numZoomLevels: 19,
				units: 'm',
				projection: new OpenLayers.Projection("EPSG:900913"),
				displayProjection: new OpenLayers.Projection("EPSG:4326")
			} );
 
			// Define the map layer
			// Here we use a predefined layer that will be kept up to date with URL changes
			layerMapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
			map.addLayer(layerMapnik);
			layerCycleMap = new OpenLayers.Layer.OSM.CycleMap("CycleMap");
			map.addLayer(layerCycleMap);
			layerMarkers = new OpenLayers.Layer.Markers("Markers");
			map.addLayer(layerMarkers);
		
			var point1; 
			var point2;
			var vector = new OpenLayers.Layer.Vector("LineSegment",{styleMap: styleMap});		
			var i;
			
			for(i = 1; i < points.length; i++) {
			 point1 = new OpenLayers.Geometry.Point(points[i-1][0],points[i-1][1]).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
			 point2 = new OpenLayers.Geometry.Point(points[i][0],points[i][1]).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
			 vector.addFeatures([new OpenLayers.Feature.Vector(new OpenLayers.Geometry.LineString([point1, point2 ]))]);
			}
		
			map.addLayers([vector]);
 
			var lonLat = new OpenLayers.LonLat(lon, lat).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
			map.setCenter(lonLat, zoom);
 
			var size = new OpenLayers.Size(21, 25);
			var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
			var icon = new OpenLayers.Icon('http://www.openstreetmap.org/openlayers/img/marker.png',size,offset);
			layerMarkers.addMarker(new OpenLayers.Marker(lonLat,icon));
		}