<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="resource.user.AbstractDisplayUser" %>

<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
	List<AbstractDisplayUser> userList = (List<AbstractDisplayUser>) request.getAttribute("userList");
	if(userList == null)
		userList = new ArrayList<AbstractDisplayUser>();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | User Administration Page</title>
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>

<h2>User List</h2>
  <table>
   <tr>
    <th>UserId</th>
    <th>User name</th>
    <th>User level</th>
    <th>First name</th>
    <th>Surname</th>
    <th>Email Address</th>
    <th>Actions</th>
   </tr>
<%
for(AbstractDisplayUser user : userList){
	out.println("<tr>\n"+
				 "<td>"+user.getUserId()+"</td>\n"+
				 "<td><a href=\"/AdventureHunter/User/View/?userId="+user.getUserId()+"\">"+user.getUserName()+"</a></td>\n"+
				 "<td>"+user.getUserLevel()+"</td>\n"+
				 "<td>"+user.getFirstName()+"</td>\n"+
				 "<td>"+user.getSurname()+"</td>\n"+
				 "<td>"+user.getEmailAddress()+"</td>\n"+
				 "<td><a href =\"/AdventureHunter/Administration/User/Archive/?userId="+user.getUserId()+"\">[Archive]</a>"+
				 "<a href =\"/AdventureHunter/Administration/User/ChangeUserLevel/?userId="+user.getUserId()+"\">[Change user level]</a>"+
				 "<a href =\"/AdventureHunter/Administration/User/Edit/?userId="+user.getUserId()+"\">[edit info]</a>"+
				 "<a href =\"/AdventureHunter/Administration/User/SendEmail/?userId="+user.getUserId()+"\">[email user]</a></td>\n"+
				"</tr>");
}
   %>  
  </table>
  <br />
<%=HTMLFactory.getBodyEnd() %>  
</body>
</html>