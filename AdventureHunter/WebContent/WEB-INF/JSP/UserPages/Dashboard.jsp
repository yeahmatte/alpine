<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory" %>
<%@page import="utils.MessageContainer" %>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="resource.activity.ActivityInterface" %>
<%@page import="resource.activity.ActivityGenerator" %>
<%@page import="java.util.List"%>
<%@page import="utils.Converter"%>
<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Edit User Information</title>
<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
	<%=HTMLFactory.getBodyStart(sessionUser) %>
	User Stuff
	<br />
	<br />
	<div>
		<h3>Your latest activities</h3>
		<ul>
<%
 List<ActivityInterface> activityList = ActivityGenerator.getInstance().getUserLatestActivities((int) sessionUser.getUserId());
 for(ActivityInterface activity : activityList) {
	%>
		<li><a href="/AdventureHunter/Activity/View/?activityId=<%=activity.getActivityId() %>"><%=activity.getName() %></a> - <%=Converter.CalendarToDbString(activity.getStartTime()) %> </li>
<% 
 }
%>
		</ul>
	</div>

	<%=HTMLFactory.getBodyEnd() %>
</body>
</html>