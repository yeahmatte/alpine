<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractUserAccount" %>
<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("ErrorContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	
	AbstractUserAccount userAccount = (AbstractUserAccount) request.getAttribute("UserAccount");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Error - 404</title>
<link href="../CSS/Forms/editUserInfoForm.css" rel="stylesheet" type="text/css">
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>

</body>
</html>