<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="utils.Converter"%>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="resource.activity.ActivityInterface" %>
<%@page import="model.gpsanalyzer.analyzerContainers.DistanceContainer" %>
<%@page import="model.gpsanalyzer.analyzerContainers.ElevationContainer" %>
<%@page import="model.gpsanalyzer.analyzerContainers.SpeedContainer" %>
<%@page import="model.gpsanalyzer.analyzerContainers.TimeContainer" %>
<%@page import="model.gpsanalyzer.analyzerContainers.MapPointContainer" %>

<%
	MessageContainer errorContainer = (MessageContainer) request.getAttribute("ErrorContainer");
	if (errorContainer == null)
		errorContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
	ActivityInterface activity = (ActivityInterface) request.getAttribute("Activity");
	
	DistanceContainer distanceContainer;
	ElevationContainer elevationContainer;
	SpeedContainer	speedContainer;
	TimeContainer timeContainer;
	MapPointContainer mapContainer;
			
	if(activity == null) {
		response.sendRedirect("/AdventureHunter/");
	}
	
	distanceContainer = (DistanceContainer) activity.getContainer("Distance");
	elevationContainer = (ElevationContainer) activity.getContainer("Elevation");
	speedContainer = (SpeedContainer) activity.getContainer("Speed");
	timeContainer = (TimeContainer) activity.getContainer("Time");
	mapContainer = (MapPointContainer) activity.getContainer("Map");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | <%=activity.getName() %></title>
	<%=HTMLFactory.getHeadMeta() %>

	<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
	<script src="http://www.openstreetmap.org/openlayers/OpenStreetMap.js"></script>
	<script type="text/javascript">
		var lat = <%=activity.getStartLatitude() %>;
		var lon = <%=activity.getStartLongitude() %>;
		var zoom = 13;
		var points = [<%=mapContainer.getGraphLabels() %>];
	 </script>
	 <script src="/AdventureHunter/JS/Maps/mapInit.js"></script>
	 <script src="/AdventureHunter/JS/Graphs/Chart.js"></script>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>
  
  
  	<table>
		 <tr>
		  <th> Attribute </th>
		  <th> Value </th>
		 </tr>
		 <tr>
		  <td> Activity name: </td>
		  <td> <%=activity.getName() %> </td>
		 </tr>
		 <tr>
		  <td>Athlete</td>
		  <td><%=activity.getUserId() %></td>
		 </tr>
		 <tr>
		  <td> Date: </td>
		  <td> <%=Converter.CalendarToDbString(activity.getStartTime()) %> </td>
		 </tr>
		 <tr>
		  <td> Total Time: </td>
		  <td> <%=Converter.doubleToTimeString(timeContainer.getTotalTime()) %> </td>
		 </tr>
		 <tr>
		  <td> Time Elapsed: </td>
		  <td> <%=Converter.doubleToTimeString(timeContainer.getTotalElapsedTime()) %> </td>
		 </tr>
		 <tr>
		  <td> Activity Type: </td>
		  <td> <%=activity.getActivityTypeName() %> </td>
		 </tr>
		 <tr>
		  <td> Total length: </td>
		  <td> <%=distanceContainer.getTotalDistance() %> </td>
		 </tr>
		 <tr>
		  <td> Total Ascent: </td>
		  <td> <%=elevationContainer.getTotalElevationAscended() %> meters </td>
		 </tr>
		 <tr>
		  <td> Total descent: </td>
		  <td> <%=elevationContainer.getTotalElevationDescended() %> meters </td>
		 </tr>
		 <tr>
		  <td> Max speed </td>
		  <td> <%=speedContainer.getMaxSpeed() %> km/h </td>
		 </tr>
		</table>
		
		<hr />
		
		<h3> Elevation curve </h3>
		<div style="width: 60%">
			<div>
				<canvas id="ElevationCurve" height="1" width="6"></canvas>
			</div>
		</div>

		<hr />
		
		<h3> Speed curve </h3>
		<div style="width: 60%">
			<div>
				<canvas id="SpeedCurve" height="1" width="6"></canvas>
			</div>
		</div>

		<hr />		 
		<h3> Map </h3>
		<!-- define a DIV into which the map will appear. Make it take up the whole window -->
		<div style="width:500px; height:500px" id="map"></div>
		
		
		<hr />
		<h3>Splits</h3>
		<table>
		 <tr>
		  <th> KM </th>
		  <th>Time</th>
		 </tr>
		 <tr>
		  <td> 1 </td>
		  <td> 1:33 </td>
		 </tr>
		</table>
		
		<hr />
		<h3> Images </h3>
		
		<img src="test" alt="test" width="640" height="480" />
		
		<hr />
		<h3> Videos </h3>
		
		Youtube
		
		<hr />
  
  
  
<%=HTMLFactory.getBodyEnd() %>
	<script>
		var elevationData = {
			labels : [<%=timeContainer.getGraphLabels() %>],
			datasets : [
				{
					label: "Elevation Data set",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [<%=elevationContainer.getGraphLabels() %>]
				}
			]
		}
		
		var speedData = {
				labels : [<%=timeContainer.getGraphLabels() %>],
				datasets : [
					{
						label: "Speed Data Set",
						fillColor : "rgba(151,187,205,0.2)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						pointHighlightFill : "#fff",
						pointHighlightStroke : "rgba(151,187,205,1)",
						data : [<%=speedContainer.getGraphLabels() %>]
					}
				]
			}
		
	window.onload = function(){
		var ctx = document.getElementById("ElevationCurve").getContext("2d");
		window.myLine = new Chart(ctx).Line(elevationData, { responsive: true, pointDot : false, pointHitDetectionRadius : 0.01,scaleShowGridLines : true});
		
		ctx = document.getElementById("SpeedCurve").getContext("2d");
		window.myLine = new Chart(ctx).Line(speedData, { responsive: true, pointDot : false, pointHitDetectionRadius : 0.01,scaleShowGridLines : true});
		
		init();
	}
	</script>  
  </body>
</html>