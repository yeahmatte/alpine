<%@page import="utils.exceptions.ObjectNotCreatedException" %>
<%@page import="resource.factory.HTMLFactory" %>
<%@page import="utils.MessageContainer" %>
<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
if(messageContainer==null)
	messageContainer = new MessageContainer();
%>  

<html>
<head>
	<title>Adventure Hunter : Login</title>
	<link href="/AdventureHunter/CSS/Forms/loginForm.css" rel="stylesheet" type="text/css">
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body background="/AdventureHunter/Img/LoginFormBg.png">
	<table class="mainouter" align="center" border="0" cellpadding="10"
		cellspacing="0" width="95%">
		<tbody>
			<tr>
				<td class="outer" align="center">

					<div id="login">
						<img src="/AdventureHunter/Img/Logo/AdventureHunterLogo_top.png" alt="Adventure Hunter logo">
						<p>
						<%=messageContainer.getMsg("loginError") %>
						</p>
						
						<form method="POST" action="/AdventureHunter/User/Login/">
							<p>
								User Name: <br /> 
								<input class="textfield" size="40"	name="username" maxlength="24" type="text" placeholder="Username"> <br />
								Password: <br /> 
								<input class="textfield" size="40" name="password" maxlength="30" type="password" placeholder="Password">
							</p>
							<input value="Sign in" class="btn" type="submit">
						</form>
						<div id="text">
							<a href="/AdventureHunter/User/Registration/">New User</a> 
							&nbsp;|&nbsp;
							<a href="/AdventureHunter/information/AboutCookies/">About Cookies</a> 
							&nbsp;|&nbsp; 
							<a href="/AdventureHunter/User/forgotPassword/">Forgot your password?</a>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="bottom" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr valign="top">
			</tr>
		</tbody>
	</table>
</body>
</html>