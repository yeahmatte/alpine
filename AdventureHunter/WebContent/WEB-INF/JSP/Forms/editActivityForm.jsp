<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="resource.activity.ActivityInterface" %>

<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
	
	ActivityInterface activity = (ActivityInterface) request.getAttribute("activity");
	
	String[] activityTypes = {"Running","Skiing","Cycling"};
	
%>
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Edit activity</title>
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>

<br />
<form action="/AdventureHunter/Activity/Edit/" method="post">
		Activity name: <input type="TEXT" name="activityName" value="<%=activity.getName() %>" placeholder="Activity Name" required />
		<span class="error"><%=messageContainer.getMsg("ActivityNameError") %></span>
		<br />
		
		Activity comment: <input type="TEXT" name="activityDesc" value="<%=activity.getDescription() %>" placeholder="Comment about the activity"/>
		<span class="error"><%=messageContainer.getMsg("ActivityDescError") %></span>
		<br />
		
		Activity Type: 
		 <select name="activityType">
		  <%
		  for(String type : activityTypes) {
		  %>
			<option value="<%=type %>" <% if(type.compareTo(activity.getActivityTypeName())==0){ %>selected <%}%>><%=type %></option>
			
		<%	  
		  }
		  %>
		 </select>
		<span class="error"><%=messageContainer.getMsg("ActivityDescError") %></span>
		<br />
		
		<input type="Submit" value="Edit information" />
</form>

<%=HTMLFactory.getBodyEnd() %>
</body>
</html>