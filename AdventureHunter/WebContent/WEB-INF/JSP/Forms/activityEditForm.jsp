<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="resource.user.AbstractDisplayUser" %>

<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
%>
<title><%=HTMLFactory.getPageTitle() %> | Edit activity</title>
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>
Edit activity

<%=HTMLFactory.getBodyEnd() %>  
</body>
</html>