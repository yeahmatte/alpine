<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractSessionUser" %>
<%@page import="resource.activity.ActivityInterface" %>

<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
	ActivityInterface activity = (ActivityInterface) request.getAttribute("Activity");
%>
<title><%=HTMLFactory.getPageTitle() %> | Add image to activity</title>
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>

Only jpg images work ATM <br />
<span class="error"><%=messageContainer.getMsg("ActionPerformed") %></span>
<form action="/AdventureHunter/Activity/ImageUpload/?activityId=<%=activity.getActivityId() %>" method="post" enctype="multipart/form-data">
 New image: <input type="file" name="newImage"> <br />
 <input type="submit" value="Upload new image" />
</form>

<%=HTMLFactory.getBodyEnd() %>  
</body>
</html>