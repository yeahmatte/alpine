<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Registration</title>
	<%=HTMLFactory.getHeadMeta() %>
	<link href="/AdventureHunter/CSS/Forms/registrationForm.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="testbox">
  <h1>Register a new user account</h1>
   <span><%=messageContainer.getMsg("GeneralError") %></span>
  <form method="POST" action="/AdventureHunter/User/Registration/">
   <hr />
   
   <span><%=messageContainer.getMsg("UserNameError") %></span><br />
   <label id="icon" for="username"><i class="icon-user"></i></label>
   <input type="text" name="username" id="username" placeholder="Username" required />
   
   <span><%=messageContainer.getMsg("EmailError") %></span><br />
   <label id="icon" for="email"><i class="icon-envelope "></i></label>
   <input type="text" name="email" id="email" placeholder="Email" required/>

   <span><%=messageContainer.getMsg("FirstNameError") %></span><br />
   <label id="icon" for="name"><i class="icon-name"></i></label>
   <input type="text" name="firstname" id="name" placeholder="First name" required />
  
     <span><%=messageContainer.getMsg("SurnameError") %></span><br />
  <label id="icon" for="name"><i class="icon-name"></i></label>
   <input type="text" name="surname" id="name" placeholder="Surname" required />
  
  <span><%=messageContainer.getMsg("PasswordError") %></span><br />
  <label id="icon" for="password"><i class="icon-shield"></i></label>
  <input type="password" name="password" id="name" placeholder="Password" required />
  
  <label id="icon" for="password"><i class="icon-shield"></i></label>
  <input type="password" name="password2" id="password2" placeholder="Re-enter password" required />
  
  <label id="icon" for="password"><i class="icon-shield"></i></label>
  <input type="checkbox" name="userAgree" value="agreed" id="userAgree"  required />
   <p>I agree to the <a href="#">terms and condition</a>.</p>
   <hr />
  <div class="gender">
    <input type="radio" value="None" id="male" name="gender" checked/>
    <label for="male" class="radio" chec>Male</label>
    <input type="radio" value="None" id="female" name="gender" />
    <label for="female" class="radio">Female</label>
   </div> 
   <input type="submit" class="button" value="Register">
  </form>
</div>

</body>
</html>