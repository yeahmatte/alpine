<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractSessionUser" %>

<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
%>
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Upload Activity</title>
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>

Right now you can only upload GPX files! <br />
<%=messageContainer.getMsg("UploadError") %>
<br />
<form action="/AdventureHunter/Activity/Upload/" method="post" enctype="multipart/form-data">
 New activity: <input type="file" name="newActivity"> <br />
 <input type="submit" value="Upload new activity" />
</form>

<%=HTMLFactory.getBodyEnd() %>
</body>
</html>