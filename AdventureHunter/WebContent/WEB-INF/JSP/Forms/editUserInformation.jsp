<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="resource.factory.HTMLFactory"%>
<%@page import="utils.MessageContainer"%>
<%@page import="resource.user.AbstractUserAccount" %>
<%@page import="resource.user.AbstractSessionUser" %>
<%
	MessageContainer messageContainer = (MessageContainer) request.getAttribute("MessageContainer");
	if (messageContainer == null)
		messageContainer = new MessageContainer();
	
	AbstractUserAccount userAccount = (AbstractUserAccount) request.getAttribute("UserAccount");
	AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><%=HTMLFactory.getPageTitle() %> | Edit User Information</title>
<link href="/AdventureHunter/CSS/Forms/editUserInfoForm.css" rel="stylesheet" type="text/css">
	<%=HTMLFactory.getHeadMeta() %>
</head>
<body>
<%=HTMLFactory.getBodyStart(sessionUser) %>
	<!-- Form for user info -->
	<form action="/AdventureHunter/User/Edit/" method="POST">
		<input type="Hidden" name="InfoChange" value="InfoChange" />
		First name: <input type="TEXT" name="firstname" value="<%=userAccount.getFirstName() %>" placeholder="First Name" required />
		<span class="error"><%=messageContainer.getMsg("FirstNameError") %></span>
		<br />
		First name: <input type="TEXT" name="surname" value="<%=userAccount.getSurname() %>" placeholder="Surname" required />
		<span class="error"><%=messageContainer.getMsg("SurnameError") %></span>
		<br />
		<input type="Submit" value="Change user info" />
	</form> 
	<hr />
	<!-- Form for email -->
	<form action="/AdventureHunter/User/Edit/" method="POST">
		<input type="Hidden" name="EmailChange" value="PasswordChange" />
		Email address: <input type="TEXT" name="email" value="<%=userAccount.getEmailAddress() %>" placeholder="Email Address" required />
		<span class="error"><%=messageContainer.getMsg("EmailError") %></span>
		<br />
		Password: <input type="PASSWORD" name="password" value="" placeholder="password" required />
		<span class="error"><%=messageContainer.getMsg("EmailPasswordError") %></span>
		<br />
		<input type="Submit" value="Change email address" />
	</form>
	<hr />	
	<!-- Form for password change -->
	<form action="/AdventureHunter/User/Edit/" method="POST">
		<input type="Hidden" name="PasswordChange" value="EmailChange" />
		New password: <input type="PASSWORD" name="newPassword1" value="" placeholder="New password" required />
		<span class="error"><%=messageContainer.getMsg("NewPasswordError") %></span>
		<br />
		Re-enter new password: <input type="PASSWORD" name="newPassword2" value="" placeholder="New password" required />
		<span class="error"><%=messageContainer.getMsg("MatchingPasswordError") %></span>
		<br />
		Current password: <input type="PASSWORD" name="password" value="" placeholder="Current password" required />
		<span class="error"><%=messageContainer.getMsg("CurrentPasswordError") %></span>		
		<br />
		<input type="Submit" value="Change password" />
	</form>
<%=HTMLFactory.getBodyEnd() %>
</body>
</html>