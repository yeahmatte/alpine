package servlet.basicPages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import servlet.ServletTemplate;

/**
 * Servlet implementation class FrontPage
 */
@WebServlet("/Main/")
public class FrontPage extends ServletTemplate {
	private static final long serialVersionUID = 1L;
    private static final String frontPageJsp = "/WEB-INF/JSP/InformationPages/frontPage.jsp";   
	
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public FrontPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//AbstractSessionUser sessionUser = getSessionUser(request);
		
		RequestDispatcher rd = request.getRequestDispatcher(frontPageJsp);  
		rd.forward(request, response);
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}
}
