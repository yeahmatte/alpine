package servlet.user.basicFunctions;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import resource.user.UserGenerator;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.PatternValidator;
import utils.exceptions.InactiveUserException;
import utils.exceptions.ObjectNotFoundException;
import utils.exceptions.PasswordDidnotMatchException;

/**
 * Allows the users to login.
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.2
 */
@WebServlet("/User/Login/")
public class Login extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String loginFormUrl = "/WEB-INF/JSP/Forms/loginForm.jsp";
	private static final String successRedirectURL = "/"+projectName+"/User/Dashboard/";
	private static final String alreadyLoggedInURL = "/"+projectName+"/User/Dashboard/";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);

		if(sessionUser.isGuest()) {
			RequestDispatcher rd=request.getRequestDispatcher(loginFormUrl);  
			rd.forward(request, response);
		}else{
			response.sendRedirect(alreadyLoggedInURL);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MessageContainer messageContainer = new MessageContainer();
		request.setAttribute("MessageContainer", messageContainer);

		String userName = getStringParameter(request,"username");
		String password = getStringParameter(request,"password");

		boolean errorHasBeenFound = false;
		errorHasBeenFound = (inputCheck(userName,"username",PatternValidator.checkUserName(userName),"UserNameError", messageContainer) || errorHasBeenFound);
		errorHasBeenFound = (inputCheck(password,"password",PatternValidator.checkPassword(password),"PasswordError", messageContainer) || errorHasBeenFound);

		if(!errorHasBeenFound) {
			try {
				AbstractSessionUser newSessionUser = UserGenerator.getInstance().getSessionUserFromLogin(userName, password);
				request.getSession().setAttribute("SessionUser", newSessionUser);
				response.sendRedirect(successRedirectURL);
			} catch (PasswordDidnotMatchException | ObjectNotFoundException | InactiveUserException e1){
				messageContainer.addMsg("loginError", "User Name and Password didn't match");
				System.out.println("Error: "+e1.getMessage());
				RequestDispatcher rd = request.getRequestDispatcher(loginFormUrl);  
				rd.forward(request, response);
			}
		}else {
			messageContainer.addMsg("loginError", "Incorrect user credentials");
			RequestDispatcher rd = request.getRequestDispatcher(loginFormUrl);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		return "Login";
	}

}
