package servlet.user.basicFunctions;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import resource.user.AbstractUserAccount;
import resource.user.UserGenerator;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.PatternValidator;

/**
 * Allows the user to edit her information.
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.2
 */
@WebServlet("/User/Edit/")
public class EditUserInformation extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String editUserInfoFormJsp = "/WEB-INF/JSP/Forms/editUserInformation.jsp";
	private static final String successPage = "/"+projectName+"/User/Dashboard/";

	/**
	 * @see ServletTemplate#ServletTemplate()
	 */
	public EditUserInformation() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()){
			AbstractUserAccount userAccount = UserGenerator.getInstance().getUserAccount(sessionUser.getUserId());
			request.setAttribute("UserAccount", userAccount);
			RequestDispatcher rd=request.getRequestDispatcher(editUserInfoFormJsp);  
			rd.forward(request, response);
		} else {
			RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()) {
			MessageContainer messageContainer = new MessageContainer();
			request.setAttribute("MessageContainer", messageContainer);
			AbstractUserAccount userAccount = UserGenerator.getInstance().getUserAccount(sessionUser.getUserId());
			request.setAttribute("UserAccount", userAccount);

			//Simple if and method calls to make this method easier to read
			if(request.getParameter("InfoChange")!= null ) {
				editUserInfo(request,response,sessionUser,userAccount,messageContainer);
			} else if( request.getParameter("EmailChange") != null ) {
				editUserEmail(request,response,sessionUser,userAccount,messageContainer);
			} else if( request.getParameter("PasswordChange") != null ) {
				editUserPassword(request,response,sessionUser,userAccount,messageContainer);
			} else {
				RequestDispatcher rd=request.getRequestDispatcher(editUserInfoFormJsp);  
				rd.forward(request, response);
			}
		} else {
			RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param sessionUser
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void editUserInfo(HttpServletRequest request, HttpServletResponse response, AbstractSessionUser sessionUser, AbstractUserAccount userAccount, MessageContainer messageContainer) throws ServletException, IOException {
		String firstName 	= getStringParameter(request,"firstname");
		String surname 		= getStringParameter(request,"surname");
		boolean errorHasBeenFound = false;
		errorHasBeenFound = (inputCheck(firstName,"first name",PatternValidator.checkFirstName(firstName),"FirstNameError", messageContainer) || errorHasBeenFound);
		errorHasBeenFound = (inputCheck(surname,"surname",PatternValidator.checkSurname(surname),"SurnameError", messageContainer) || errorHasBeenFound);

		if(!errorHasBeenFound) {
			userAccount.setFirstName(firstName);
			userAccount.setSurname(surname);

			userAccount.saveUserAccount();

			response.sendRedirect(successPage);
		}else{

			RequestDispatcher rd = request.getRequestDispatcher(editUserInfoFormJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param sessionUser
	 * @param messageContainer
	 * @throws ServletException
	 * @throws IOException
	 */
	private void editUserEmail(HttpServletRequest request, HttpServletResponse response, AbstractSessionUser sessionUser, AbstractUserAccount userAccount, MessageContainer messageContainer) throws ServletException, IOException {
		String email 	= getStringParameter(request,"email");
		String password = getStringParameter(request,"password");
		boolean errorHasBeenFound = false;
		errorHasBeenFound = (inputCheck(email,"email address",PatternValidator.checkEmailAddress(email),"EmailError", messageContainer) || errorHasBeenFound);
		errorHasBeenFound = (inputCheck(password,"password",PatternValidator.checkPassword(password),"EmailPasswordError", messageContainer) || errorHasBeenFound);

		if (!userAccount.checkPassword(password)){
			errorHasBeenFound = true;
			messageContainer.addMsg("EmailPasswordError", "Wrong password");
		}

		if (!errorHasBeenFound) {
			userAccount.setEmailAddress(email);
			userAccount.saveUserAccount();
			response.sendRedirect(successPage);
		} else {
			RequestDispatcher rd = request.getRequestDispatcher(editUserInfoFormJsp);  
			rd.forward(request, response);
		}

	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param sessionUser
	 * @param messageContainer
	 * @throws ServletException
	 * @throws IOException
	 */
	private void editUserPassword(HttpServletRequest request, HttpServletResponse response, AbstractSessionUser sessionUser, AbstractUserAccount userAccount, MessageContainer messageContainer) throws ServletException, IOException {
		String newPassword1		= getStringParameter(request,"newPassword1");
		String newPassword2		= getStringParameter(request,"newPassword2");
		String currentPassword 	= getStringParameter(request,"password");
		boolean errorHasBeenFound = false;
		errorHasBeenFound = (inputCheck(currentPassword,"current password",PatternValidator.checkPassword(currentPassword),"CurrentPasswordError", messageContainer) || errorHasBeenFound);
		errorHasBeenFound = (inputCheck(newPassword1,"new password",PatternValidator.checkPassword(newPassword1),"NewPasswordError", messageContainer) || errorHasBeenFound);
		errorHasBeenFound = (inputCheck(newPassword2,"re-entered password",PatternValidator.checkPassword(newPassword2),"MatchingPasswordError", messageContainer) || errorHasBeenFound);

		if(!newPassword1.equals(newPassword2)){
			messageContainer.addMsg("MatchingPasswordError", "The two passwords must match");
			errorHasBeenFound = true;
		}
		if(!userAccount.checkPassword(currentPassword)){
			errorHasBeenFound = true;
			messageContainer.addMsg("CurrentPasswordError", "Wrong password");
		}

		if(!errorHasBeenFound) {
			userAccount.setPassword(newPassword1);
			userAccount.saveUserAccount();
			response.sendRedirect(successPage);
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(editUserInfoFormJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * 
	 */
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}
}
