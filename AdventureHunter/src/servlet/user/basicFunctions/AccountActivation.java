package servlet.user.basicFunctions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.ServletTemplate;

/**
 * Allows the user to activate her account
 * after the registration. The servlet excpets
 * a activation code as a get variable.
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.1
 */
@WebServlet("/User/AccountActivation")
public class AccountActivation extends ServletTemplate {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public AccountActivation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
