package servlet.user.basicFunctions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import resource.user.UserGenerator;
import servlet.ServletTemplate;

/**
 * Allows the users to log out.
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.1
 */
@WebServlet("/User/Logout/")
public class Logout extends ServletTemplate {
	private static final long serialVersionUID = 1L;
    private static final String logoutRedirectURL = "/"+projectName+"/";
	
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public Logout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()){
			request.getSession().setAttribute("SessionUser", UserGenerator.getInstance().getNewSessionGuest());
		}
		response.sendRedirect(logoutRedirectURL);
	}

	/**
	 * 
	 */
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}
}
