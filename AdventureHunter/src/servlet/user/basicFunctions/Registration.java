package servlet.user.basicFunctions;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import resource.user.AbstractUserAccount;
import resource.user.UserGenerator;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.PatternValidator;
import utils.PostOffice;

/**
 * Servlet implementation the registration process for the system.
 * 
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.2
 */
@WebServlet("/User/Registration/")
public class Registration extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String registrationFormUrl = "/WEB-INF/JSP/Forms/registrationForm.jsp";
	private static final String registrationSuccessfulUrl = "/WEB-INF/JSP/InformationPages/registrationSuccessful.jsp";
	private static final String alreadyLoggedInURL = "/"+projectName+"/User/Dashboard/";
	
	/**
	 * @see ServletTemplate#ServletTemplate()
	 */
	public Registration() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		System.out.println(sessionUser.toString());
		if(sessionUser.isGuest()){
			RequestDispatcher rd = request.getRequestDispatcher(registrationFormUrl);  
			rd.forward(request, response);
		}else{
			response.sendRedirect(alreadyLoggedInURL);
		}
	}
	

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		MessageContainer messageContainer = new MessageContainer();
		request.setAttribute("MessageContainer", messageContainer);
		
		if(sessionUser.isGuest()){
			String username 	= (String) request.getParameter("username");
			String emailAddress = (String) request.getParameter("email");
			String firstName 	= (String) request.getParameter("firstname");
			String surname 		= (String) request.getParameter("surname");
			String password 	= (String) request.getParameter("password");
			String password2 	= (String) request.getParameter("password2");
			boolean userAgree 	= request.getParameter( "userAgree" ) != null;

			boolean errorHasBeenFound = false;
			//This became less clear but it were necessary to avoid duplicate code
			errorHasBeenFound = (inputCheck(username,"username",PatternValidator.checkUserName(username),"UserNameError", messageContainer) || errorHasBeenFound);
			errorHasBeenFound = (inputCheck(emailAddress,"email address",PatternValidator.checkEmailAddress(emailAddress),"EmailError", messageContainer) || errorHasBeenFound);
			errorHasBeenFound = (inputCheck(firstName,"first name",PatternValidator.checkFirstName(firstName),"FirstNameError", messageContainer) || errorHasBeenFound);
			errorHasBeenFound = (inputCheck(surname,"surname",PatternValidator.checkSurname(surname),"SurnameError", messageContainer) || errorHasBeenFound);
			errorHasBeenFound = (inputCheck(password,"password",PatternValidator.checkPassword(password),"PasswordError", messageContainer) || errorHasBeenFound);
		
			if(!UserGenerator.getInstance().isUserNameAvailable(username.trim())){
				messageContainer.addMsg("UserNameError", "The username is already taken");
				errorHasBeenFound = true;
			}
			/*if(UserAccountGenerator.isEmailAddressAvailable(emailAddress.trim())){
				errorContainer.addMsg("UserNameError", "The username is already taken");
				error = true;
			}*/
			if(!password.equals(password2)){
				messageContainer.addMsg("PasswordError", "The two passwords must match");
				errorHasBeenFound = true;
			}
			if(!userAgree){
				messageContainer.addMsg("UserAgreeError", "You must read the user agreement");
				errorHasBeenFound = true;
			}

			if(!errorHasBeenFound){
				AbstractUserAccount newUser = UserGenerator.getInstance().createNewUserAccount(username, password, firstName, surname, emailAddress);
				PostOffice.sendWelcomeMail(newUser);
				RequestDispatcher rd = request.getRequestDispatcher(registrationSuccessfulUrl);  
				rd.forward(request, response);
			}else{
				RequestDispatcher rd = request.getRequestDispatcher(registrationFormUrl);  
				rd.forward(request, response);
			}
		}else{
			response.sendRedirect(alreadyLoggedInURL);
		}
	}

	/**
	 * 
	 */
	public String getTitle() {
		// TODO Auto-generated method stub
		return "User registration";
	}

}
