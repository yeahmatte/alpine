package servlet.user.basicPages;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import servlet.ServletTemplate;

/**
 * Servlet implementation class UserDashboard
 */
@WebServlet("/User/Dashboard/")
public class UserDashboard extends ServletTemplate {
	private static final long serialVersionUID = 1L;
    private static final String UserDashboardJsp = "/WEB-INF/JSP/UserPages/Dashboard.jsp";
	
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public UserDashboard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()){
			RequestDispatcher rd=request.getRequestDispatcher(UserDashboardJsp);  
			rd.forward(request, response);
		} else {
			RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "Dashboard";
	}

}
