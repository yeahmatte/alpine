package servlet.administration.userAdministration;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.system.UserLevelEnum;
import resource.user.AbstractSessionUser;
import resource.user.AbstractUserAccount;
import resource.user.UserGenerator;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.exceptions.ObjectNotFoundException;

/**
 * Allows the user to archive other users.
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.1
 */
@WebServlet("/Administration/User/Archive/")
public class ArchiveUser extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String userAdministrationPageJsp = "/WEB-INF/JSP/AdministrationPages/userAdministrationPage.jsp";

	/**
	 * @see ServletTemplate#ServletTemplate()
	 */
	public ArchiveUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		MessageContainer messageContainer = new MessageContainer();
		request.setAttribute("MessageContainer", messageContainer);

		if(sessionUser.getUserLevel() >= UserLevelEnum.ADMINISTRATOR.getUserLevel()){
			int userId = getIntParameter(request,"userId");
			try {
				AbstractUserAccount userToArchive = UserGenerator.getInstance().getUserAccount(userId);
				if(sessionUser.getUserLevel() > userToArchive.getUserLevel()) {
					userToArchive.archive();
					messageContainer.addMsg("ActionPerformed", "The user is now archived");
				}else {
					messageContainer.addMsg("ActionPerformed", "You dont have authorithy to perform this action");					
				}
			} catch ( ObjectNotFoundException e) {
				messageContainer.addMsg("ActionPerformed", "User not found");
			}

			RequestDispatcher rd = request.getRequestDispatcher(userAdministrationPageJsp);  
			rd.forward(request, response);

		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
