package servlet.administration.userAdministration;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.system.UserLevelEnum;
import resource.user.AbstractSessionUser;
import resource.user.AbstractUserAccount;
import resource.user.UserGenerator;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.PatternValidator;
import utils.exceptions.ObjectNotFoundException;

/**
 * Allows an administrator to edit other users basic
 * information.
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.1
 */
@WebServlet("/Administration/User/Edit/")
public class AdministrationEditUserInformation extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String administratorEditUserInformationJsp = "/WEB-INF/JSP/AdministrationPages/administratorEditUserInformation.jsp";
	private static final String unknownUserJsp = "/WEB-INF/JSP/ErrorPages/unkownUser.jsp";

	/**
	 * @see ServletTemplate#ServletTemplate()
	 */
	public AdministrationEditUserInformation() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		if(sessionUser.getUserLevel() >= UserLevelEnum.ADMINISTRATOR.getUserLevel()){
			int userId = getIntParameter(request,"userId");
			try {
				AbstractUserAccount userAccount = UserGenerator.getInstance().getUserAccount(userId);
				request.setAttribute("UserAccount", userAccount);

				RequestDispatcher rd = request.getRequestDispatcher(administratorEditUserInformationJsp);  
				rd.forward(request, response);
			} catch (ObjectNotFoundException e) {
				RequestDispatcher rd = request.getRequestDispatcher(unknownUserJsp);  
				rd.forward(request, response);	
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		if(sessionUser.getUserLevel() >= UserLevelEnum.ADMINISTRATOR.getUserLevel()){
			int userId = getIntParameter(request,"userId");
			try {
				AbstractUserAccount userAccount = UserGenerator.getInstance().getUserAccount(userId);
				request.setAttribute("UserAccount", userAccount);
				MessageContainer messageContainer = new MessageContainer();
				request.setAttribute("MessageContainer", messageContainer);

				String firstName 	= getStringParameter(request,"firstname");
				String surname 		= getStringParameter(request,"surname");
				boolean errorHasBeenFound = false;
				errorHasBeenFound = (inputCheck(firstName,"first name",PatternValidator.checkFirstName(firstName),"FirstNameError", messageContainer) || errorHasBeenFound);
				errorHasBeenFound = (inputCheck(surname,"surname",PatternValidator.checkSurname(surname),"SurnameError", messageContainer) || errorHasBeenFound);

				if(!errorHasBeenFound) {
					userAccount.setFirstName(firstName);
					userAccount.setSurname(surname);
					userAccount.saveUserAccount();
				}
				RequestDispatcher rd = request.getRequestDispatcher(administratorEditUserInformationJsp);  
				rd.forward(request, response);
			} catch (ObjectNotFoundException e) {
				RequestDispatcher rd = request.getRequestDispatcher(unknownUserJsp);  
				rd.forward(request, response);	
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
