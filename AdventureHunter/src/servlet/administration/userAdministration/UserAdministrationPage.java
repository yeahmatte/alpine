package servlet.administration.userAdministration;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.system.UserLevelEnum;
import resource.user.AbstractDisplayUser;
import resource.user.AbstractSessionUser;
import resource.user.UserGenerator;
import servlet.ServletTemplate;

/**
 * Servlet implementation class UserAdministrationPage
 * 
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.2
 */
@WebServlet("/Administration/User/")
public class UserAdministrationPage extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String userAdministrationPageJsp = "/WEB-INF/JSP/AdministrationPages/userAdministrationPage.jsp";
       
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public UserAdministrationPage() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		if(sessionUser.getUserLevel() >= UserLevelEnum.ADMINISTRATOR.getUserLevel()){
			int pageNo = getIntParameter(request,"page");
			List<AbstractDisplayUser> userList = UserGenerator.getInstance().getUserList(pageNo);
			request.setAttribute("userList", userList);
			RequestDispatcher rd = request.getRequestDispatcher(userAdministrationPageJsp);  
			rd.forward(request, response);
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "UserAdministration";
	}

}
