package servlet.administration.userAdministration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.ServletTemplate;

/**
 * Allows the administrators to send emails
 * to other users.
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.1
 */
@WebServlet("/Administration/User/SendEmail/")
public class SendEmailToUser extends ServletTemplate {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public SendEmailToUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
