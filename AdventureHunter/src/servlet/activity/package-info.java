/**
 * This package contains the servlets associated
 * with activity functions.
 */
/**
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
package servlet.activity;