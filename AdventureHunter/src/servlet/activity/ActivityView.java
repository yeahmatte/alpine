package servlet.activity;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.gpsanalyzer.GpsLogAnalyzer;
import resource.activity.ActivityGenerator;
import resource.activity.ActivityInterface;
import servlet.ServletTemplate;
import utils.exceptions.ObjectNotFoundException;

/**
 * Allows users to view activities.
 * @author Mattias Mellhorn
 * @since 0.5
 * @version 0.1
 */
@WebServlet("/Activity/View/")
public class ActivityView extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String unknownActivityJsp = "/WEB-INF/JSP/ErrorPages/unknownActivity.jsp";
	private static final String viewActivityJsp = "/WEB-INF/JSP/Activity/viewActivity.jsp";
	
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public ActivityView() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int activityId = getIntParameter(request,"activityId");
		ActivityInterface activity;
		try {
		  activity = ActivityGenerator.getInstance().getActivity(activityId, true);
		  
		  GpsLogAnalyzer logAnalyzer = new GpsLogAnalyzer();
		  logAnalyzer.setGpsPoints(activity.getGpsPoints());
		  logAnalyzer.setActivityType(activity.getActivityType());
		  logAnalyzer.analyze();
		  
		  request.setAttribute("Activity", activity);
		  
		  RequestDispatcher rd = request.getRequestDispatcher(viewActivityJsp);  
		  rd.forward(request, response);
		  
		} catch (ObjectNotFoundException e) {
			RequestDispatcher rd = request.getRequestDispatcher(unknownActivityJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
