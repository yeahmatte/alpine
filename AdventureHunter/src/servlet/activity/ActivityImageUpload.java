package servlet.activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import resource.activity.ActivityGenerator;
import resource.activity.ActivityInterface;
import resource.image.ImageGenerator;
import resource.system.ActivityImageSettingsEnum;
import resource.user.AbstractSessionUser;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.exceptions.ObjectNotCreatedException;

/**
 * Allows the user to upload new images to
 * her activities.
 * @author Mattias Mellhorn
 * @since 0.4
 * @version 0.1
 */
@MultipartConfig()
@WebServlet("/Activity/ImageUpload/")
public class ActivityImageUpload extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String uploadImageFormJsp = "/WEB-INF/JSP/Forms/activityUploadImageForm.jsp";
	private static final String unkownActivityJsp = "/WEB-INF/JSP/ErrorPages/unknownActivity.jsp";

	/**
	 * @see ServletTemplate#ServletTemplate()
	 */
	public ActivityImageUpload() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		if(!sessionUser.isGuest()){
			int activityId = getIntParameter(request,"activityId");
			ActivityInterface activity = ActivityGenerator.getInstance().getActivity(activityId, false);
			if(activity!=null) {
				if( (int) activity.getUserId() == (int) sessionUser.getUserId() ) {
					request.setAttribute("Activity", activity);
					RequestDispatcher rd = request.getRequestDispatcher(uploadImageFormJsp);  
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
					rd.forward(request, response);
				}
			} else {
				RequestDispatcher rd = request.getRequestDispatcher(unkownActivityJsp);  
				rd.forward(request, response);
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		MessageContainer messageContainer = new MessageContainer();
		request.setAttribute("MessageContainer", messageContainer);

		if(!sessionUser.isGuest()){
			int activityId = getIntParameter(request,"activityId");
			ActivityInterface activity = ActivityGenerator.getInstance().getActivity(activityId, false);
			request.setAttribute("Activity", activity);
			if(activity!=null) {
				if( (int) activity.getUserId() == (int) sessionUser.getUserId() ) {
					boolean errorHasBeenFound = false;
					Part filePart = request.getPart("newImage");
					if(filePart == null  || filePart.getSize()==0) {
						messageContainer.addMsg("ActionPerformed", "No file");
						errorHasBeenFound = true;
					} else if(filePart.getSize() > ActivityImageSettingsEnum.IMAGE_MAX_SIZE.getValue()) {
						messageContainer.addMsg("ActionPerformed", "File to large");
						errorHasBeenFound = true;
					}  

					//Implement this, Check for filetype
					if( !filePart.getHeaders("").contains("jpg")  ){
						System.out.println(filePart.getHeader("content-type"));
					}

					if (!errorHasBeenFound) {
						InputStream imageStream = filePart.getInputStream();
						try {
							ImageGenerator.getInstance().createNewActivityImage(activity.getActivityId(), imageStream);
							messageContainer.addMsg("ActionPerformed", "Image added");
						} catch ( ObjectNotCreatedException e) {
							messageContainer.addMsg("ActionPerformed", e.getMessage());
						}
					} 
					RequestDispatcher rd = request.getRequestDispatcher(uploadImageFormJsp);  
					rd.forward(request, response);

				} else {
					//TODO: REMOVE
					System.out.print("Hej 1");
					RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
					rd.forward(request, response);
				}
			} else {
				RequestDispatcher rd = request.getRequestDispatcher(unkownActivityJsp);  
				rd.forward(request, response);
			}
		}else{
			//TODO: REMOVE
			System.out.print("Hej 2");
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

}
