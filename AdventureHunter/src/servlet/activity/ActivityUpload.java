package servlet.activity;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import resource.activity.ActivityGenerator;
import resource.activity.ActivityInterface;
import resource.user.AbstractSessionUser;
import servlet.ServletTemplate;
import utils.MessageContainer;

/**
 * Allows user to upload new activities.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.3
 */
@MultipartConfig()
@WebServlet("/Activity/Upload/")
public class ActivityUpload extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String uploadFormJsp = "/WEB-INF/JSP/Forms/uploadActivityForm.jsp";
	private static final String editActivityUrl = "/"+projectName+"/Activity/Edit/";
	
    /**
     * @see ServletTemplate#ServletTemplate()
     */
    public ActivityUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()){
			RequestDispatcher rd=request.getRequestDispatcher(uploadFormJsp);  
			rd.forward(request, response);
		} else {
			RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = this.getSessionUser(request);
		if(!sessionUser.isGuest()){
			Part filePart = request.getPart("newActivity");
		    //String filename = getFilename(filePart);
		    if(filePart != null  && filePart.getSize()>0) {
			InputStream activityFile = filePart.getInputStream();
			
		    ActivityInterface newActivity = ActivityGenerator.getInstance().createNewActivity(activityFile, sessionUser);
		    
		    response.sendRedirect(editActivityUrl+"?activityId="+newActivity.getActivityId());
		    
		    } else {
				MessageContainer messageContainer = new MessageContainer();
				request.setAttribute("MessageContainer", messageContainer);
				messageContainer.addMsg("uploadError", "No file attached");
				RequestDispatcher rd=request.getRequestDispatcher(uploadFormJsp);  
				rd.forward(request, response);
		    }
		} else {
			RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		return "Activity upload";
	}

}
