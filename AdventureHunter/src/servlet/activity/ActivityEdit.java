package servlet.activity;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.activity.ActivityGenerator;
import resource.activity.ActivityInterface;
import resource.activity.ActivityTypeGenerator;
import resource.user.AbstractSessionUser;
import servlet.ServletTemplate;
import utils.MessageContainer;
import utils.PatternValidator;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
@WebServlet("/Activity/Edit/")
public class ActivityEdit extends ServletTemplate {
	private static final long serialVersionUID = 1L;
	private static final String editFormJsp = "/WEB-INF/JSP/Forms/activityEditForm.jsp";
	private static final String unknownActivityJsp = "/WEB-INF/JSP/ErrorPages/unknownActivity.jsp";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ActivityEdit() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		if(!sessionUser.isGuest()) {
			int activityId = getIntParameter(request,"activityId");
			ActivityInterface activity = ActivityGenerator.getInstance().getActivity(activityId, false);	
			if(activity!=null) {
				if( (int) activity.getUserId() == (int) sessionUser.getUserId() ) {
					request.setAttribute("Activity", activity);
					RequestDispatcher rd = request.getRequestDispatcher(editFormJsp);  
					rd.forward(request, response);
				} else {
					RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
					rd.forward(request, response);
				}
			} else {
				RequestDispatcher rd = request.getRequestDispatcher(unknownActivityJsp);  
				rd.forward(request, response);
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AbstractSessionUser sessionUser = getSessionUser(request);
		MessageContainer messageContainer = new MessageContainer();
		request.setAttribute("MessageContainer", messageContainer);
		
		if(!sessionUser.isGuest()){
			int activityId = getIntParameter(request,"activityId");
			ActivityInterface activity = ActivityGenerator.getInstance().getActivity(activityId, false);
			if(activity!=null) {
				if( activity.getUserId() == sessionUser.getUserId() ) {
					String activityName = getStringParameter(request,"activityName");
					String activityDesc = getStringParameter(request,"activityDesc");
					String activityType = getStringParameter(request,"activityType");
					
					boolean errorHasBeenFound = false;
					//This became less clear but it were necessary to avoid duplicate code
					errorHasBeenFound = (inputCheck(activityName,"Ativity Name",PatternValidator.checkActivityName(activityName),"ActivityNameError", messageContainer) || errorHasBeenFound);
					errorHasBeenFound = (inputCheck(activityDesc,"Activity Description",PatternValidator.checkActivityDesc(activityDesc),"ActivityDescError", messageContainer) || errorHasBeenFound);
					errorHasBeenFound = (inputCheck(activityType,"Activity Type",PatternValidator.checkActivityType(activityType),"ActivityTypeError", messageContainer) || errorHasBeenFound);
					
					if (!errorHasBeenFound) {
						activity.setName(activityName);
						activity.setDescription(activityDesc);
						activity.setActivityType(ActivityTypeGenerator.getInstance().getObjectFromString(activityType));
						activity.saveActivity();
						messageContainer.addMsg("ActionPerformed", "Changes were saved");
						
					} else {
						messageContainer.addMsg("ActionPerformed", "Error occured");
					}
					RequestDispatcher rd = request.getRequestDispatcher(editFormJsp);  
					rd.forward(request, response);
					
				} else {
					RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
					rd.forward(request, response);
				}
			} else {
				RequestDispatcher rd = request.getRequestDispatcher(unknownActivityJsp);  
				rd.forward(request, response);
			}
		}else{
			RequestDispatcher rd = request.getRequestDispatcher(UnauthuriztedAccessJsp);  
			rd.forward(request, response);
		}
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return "Edit activity";
	}

}
