package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import resource.user.AbstractSessionUser;
import resource.user.UserGenerator;
import utils.MessageContainer;
import utils.PatternValidator;

/**
 * The base for all servlets. Defines the basic method all servlets must implement.
 * 
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.1
 */
//@WebServlet("/ServletTemplate")
public abstract class ServletTemplate extends HttpServlet {
	private static final long serialVersionUID = 1L;
    protected static final String UnauthuriztedAccessJsp = "/WEB-INF/JSP/ErrorPages/unauthuritzedAccess.jsp";   
    protected ServletContext servletContext;
    protected static final String projectName = "AdventureHunter";
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletTemplate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

	/**
	 * 
	 * 
	 * @throws ServletException
	 * @throws IOException
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd=request.getRequestDispatcher(UnauthuriztedAccessJsp);  
		rd.forward(request, response);
	}
	
	/**
	 * Used for initiation
	 * @throws ServletException
	 * @see {@link HttpServlet#init(ServletConfig)}  
	 */
	public void init(ServletConfig config) throws ServletException{
		servletContext = config.getServletContext();
		super.init(config);
	}
	
	protected ServletContext getContext() {
		return servletContext;
	}
	
	protected void setup(){
		// TODO setup basic references
		
	}
	
	protected AbstractSessionUser getSessionUser(HttpServletRequest request) {
		AbstractSessionUser sessionUser  = (AbstractSessionUser) request.getSession(true).getAttribute("SessionUser");
		if(sessionUser != null)
			return sessionUser;
		else {
			sessionUser = UserGenerator.getInstance().getNewSessionGuest();
			request.getSession().setAttribute("SessionUser", sessionUser);
			return sessionUser;
		}
	}
	
    protected int getIntParameter(HttpServletRequest request, String intValue) {
    	String paramValue = request.getParameter(intValue);
    	if(paramValue == null || !PatternValidator.checkNumber(paramValue))
    		return 0;
    	return Integer.valueOf(paramValue);
    }
    
    protected String getStringParameter(HttpServletRequest request, String stringValue) {
    	String paramValue = request.getParameter(stringValue);
    	if (paramValue == null || paramValue.trim().compareTo("") == 0)
    		return "";
    	return paramValue;
    }
	
	/**
	 * 
	 * @param parameter
	 * @param parameterName
	 * @param patternCheck
	 * @param errorTag
	 * @param errorIndicator
	 */
	protected boolean inputCheck(String parameter, String parameterName, boolean patternCheck, 
							String errorTag, MessageContainer messageContainer){
		boolean errorHasBeenFound = false;
		if(parameter==null || parameter.trim().equals("")){
			messageContainer.addMsg(errorTag, "A "+parameterName+" have to be set");
			errorHasBeenFound = true;
		}else if(!patternCheck){
			messageContainer.addMsg(errorTag, "Invalid "+parameterName);
			errorHasBeenFound = true;
		}
		return errorHasBeenFound;
	}
    
	/**
	 * Returns the title of the page to be used in headers and heading
	 * No style tags shall be included
	 * @return Title of the servlet
	 */
	public abstract String getTitle();
}
