/**
 * Utils is meant to provide aid classes for the other packages
 * @author Mattias Mellhorn
 * @version 0.1
 */
package utils;