package utils;

import java.io.IOException;
import java.sql.SQLException;

public class ErrorLogger {
	public static void addLogEntry(String error){
		appendLog(error);
	}
	
	public static void addLogEntry(RuntimeException e){
		appendLog(e.toString());
	}
	
	public static void addLogEntry(SQLException e){
		appendLog(e.toString());
	}
	
	public static void addLogEntry(IOException e){
		appendLog(e.toString());
	}
	
	private static void appendLog(String logEntry){
		System.out.println("ErrorLog: "+logEntry);
	}
}
