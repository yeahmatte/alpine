package utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * This class shall handle hashing the passwords. Checks if a password is valid
 * according to the pattern set in the SRS. Can also hash a password and
 * generate new unencrypted passwords.
 * 
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */

public class PasswordHasher {

    /**
     * Returns a string that contains the salt, containing 10 alphanumerical and
     * special characters.
     * 
     * @return salt of 20 alphanumerical and special characters
     */

    public static String getNewSalt() {
        StringBuffer buffer = new StringBuffer();
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !#%&'()*+,-./.;<>=$[]{}~";
        int charactersLength = characters.length();
        int saltLength = 10;

        for (int i = 1; i <= saltLength; i++) {
            double index = Math.random() * charactersLength;
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
    }

    /**
     * Returns a string that contains a unencrypted randomly generated password,
     * containing 6 alphanumerical and special characters.
     * 
     * @return password of 6 alphanumerical and special characters
     */
    public static String generateNewUnencryptedPassword() {

        StringBuffer buffer = new StringBuffer();
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !#%&'()*+,-./.;\"<>=$[]{}|~";
        int charactersLength = characters.length();
        int passwordLength = 6;

        for (int i = 0; i <= passwordLength; i++) {
            double index = Math.random() * charactersLength;
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
    }

    /**
     * Returns a string that contains a hash of a password.
     * 
     * @param password
     *            containing the password to be hashed
     * @param salt
     *            containing a salt. If null construct random generated salt
     *            will be used
     * @return String in hexadecimal represntation of the hashed password
     */
    public static String hashPassword(String password, String salt) {
    	MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
	    	byte[] hash = messageDigest.digest((password+salt).getBytes("UTF-8"));
	    	return new String(hash, "UTF-8");
		} catch (NoSuchAlgorithmException e) {
			ErrorLogger.addLogEntry(e.toString());
			return null;
		} catch (UnsupportedEncodingException e) {
			ErrorLogger.addLogEntry(e.toString());
			return null;
		}
    }
}
