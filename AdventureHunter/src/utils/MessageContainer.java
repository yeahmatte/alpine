package utils;

import java.util.HashMap;
import java.util.Map;

public class MessageContainer {
	private Map<String,String> errorList;
	
	public MessageContainer() {
		errorList = new HashMap<String,String>();
	}
	
	public void addMsg(String msgName, String message) {
		errorList.put(msgName, message);
	}
	
	public String getMsg(String msgName) {
		if(errorList.containsKey(msgName))
			return errorList.get(msgName);
		return "";
	}
}
