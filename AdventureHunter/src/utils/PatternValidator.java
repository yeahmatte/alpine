package utils;

import java.util.regex.Pattern;

/**
 * Help class to validate input and other objects
 * Uses RegEx to validate the objects
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public class PatternValidator {
	private final static String userNamePattern = "^[a-zA-Z0-9]{5,15}$"; 
	private final static String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,50})$";
	private final static String firstNamePattern = "^[a-zA-Z - \\u00C0-\\u00FD]{2,30}$"; 
	private final static String surnamePattern = "^[a-zA-Z - \\u00C0-\\u00FD]{2,30}$";
	private final static String passwordPattern = "^[a-zA-Z0-9\\u00C0-\\u00FD]{6,30}$";
	private final static String numberPattern = "^[0-9]{1,10}$";
	
    /**
     * Returns a boolean value that decides whether the argument string
     * expression meets the conditions provided by the pattern parameter
     * 
     * @param pattern
     *            a regular expression string deciding which characters
     *            to allow
     * @param expression
     *            the string to be tested for the conditions
     * @return true if the match succeeds, otherwise false
     */
    private static boolean expressionChecker(String stringToValidate,String patternString) {
        Pattern pattern = Pattern.compile(patternString);
        return pattern.matcher(stringToValidate).matches();
    }
    
    /**
     * Static method which validates the user name against a regex
     * to catch illegal characters.
     * @param username The username to validate
     * @return a boolean with true if it's a valid user name otherwise false
     */
    public static boolean checkUserName(String username){
    	return expressionChecker(username,userNamePattern);
    }
    
    /**
     * Static method which validates the email address against a regex
     * to catch illegal characters.
     * @param emailAddress The emailAddress to validate
     * @return a boolean with true if it's a valid email address otherwise false
     */
    public static boolean checkEmailAddress(String emailAddress){
    	return expressionChecker(emailAddress, emailPattern);
    }

    /**
     * Static method which validates the first name against a regex
     * to catch illegal characters.
     * @param firstName The first name to validate
     * @return a boolean with true if it's a valid first name otherwise false
     */
    public static boolean checkFirstName(String firstName){
    	return expressionChecker(firstName,firstNamePattern);
    }
    
    /**
     * Static method which validates the surname against a regex
     * to catch illegal characters.
     * @param firstName The surname to validate
     * @return a boolean with true if it's a valid surname otherwise false
     */
    public static boolean checkSurname(String surname){
    	return expressionChecker(surname,surnamePattern);
    }
    
    /**
     * Static method which validates the password against a regex
     * to catch illegal characters, length and other formal requirements on the password .
     * @param password The password to validate
     * @return a boolean with true if it's a valid password otherwise false
     */
    public static boolean checkPassword(String password){
    	return expressionChecker(password,passwordPattern);
    }
    
    /**
     * Static method which validates that a string only contains
     * numbers.
     * @param number Input string to check
     * @return returns true if the string contains 1-10 numeric values otherwise false.
     */
    public static boolean checkNumber(String number) {
    	return expressionChecker(number,numberPattern);
    }
    
    public static boolean checkActivityName(String name) {
    	return expressionChecker(name,userNamePattern);
    }
    
    public static boolean checkActivityDesc(String description) {
    	return expressionChecker(description,userNamePattern);
    }
    
    public static boolean checkActivityType(String type) {
    	return true;
    	//return expressionChecker(description,userNamePattern);
    }
    
    
    
}
