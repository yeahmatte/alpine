package utils.exceptions;

public class PasswordDidnotMatchException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public PasswordDidnotMatchException(String message) {
		super(message);
	}
}
