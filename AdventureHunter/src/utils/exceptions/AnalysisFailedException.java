package utils.exceptions;

public class AnalysisFailedException  extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public AnalysisFailedException(String message) {
		super(message);
	}
}
