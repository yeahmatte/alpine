package utils.exceptions;

public class ObjectNotCreatedException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public ObjectNotCreatedException(String message) {
		super(message);
	}
}
