package utils;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Simple help class to do some object conversions
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public class Converter {
	
	/**
	 * Converts a sql date to Calendar
	 * @param date the to convert to Calendar
	 * @return Calendar version of the date
	 */
	public static Calendar DateToCalendar(Date date){ 
		  if(date == null)
			  return null;
		  Calendar cal = Calendar.getInstance();
		  cal.setTimeInMillis(date.getTime());
		  return cal;
	}
	
	public static String CalendarToDbString(Calendar date) {
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    return sdf.format(date.getTime());
	}
	
	public static Calendar TimeStampToCalendar(Timestamp date){ 
		  if(date == null)
			  return null;
		  Calendar cal = Calendar.getInstance();
		  cal.setTimeInMillis(date.getTime());
		  return cal;
	}
	
	/**
	 * This gives rounding error of max 2 seconds ATM
	 * @param time the time in ms to convert to HH:mm:ss
	 * @return String formated as HH:mm:ss
	 */
	public static String doubleToTimeString(double time) {
		long seconds = (long) time / 1000;
		long h = seconds / 3600;
		long remainder = seconds % 3600;
		long m = remainder / 60;
		long s = remainder % 60;

		return String.format("%d:%02d:%02d", h, m, s);
	}
	
}
