package utils;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;

import resource.user.AbstractUserAccount;

public class PostOffice {

	public static void sendMail(String recipentAddress, String subject, String msg){
		// Only Gmail will work atm
		final String username = "bicyclegaragemanager@gmail.com"; 
		final String password = "bikegarage1";					  
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
		
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("bicyclegaragemanager@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(recipentAddress));
			message.setSubject(subject);
			message.setText(msg);
			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void sendWelcomeMail(AbstractUserAccount newUserAccount){
		
	}
}
