package utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.4
 * @version 0.1
 */
public class ImageNameGenerator {

	public static String getTenCharNameHash2(String salt) {
    	MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
	    	byte[] hash = messageDigest.digest(salt.getBytes("UTF-8"));
	    	return new String(hash, "UTF-8").substring(0, 10);
		} catch (NoSuchAlgorithmException e) {
			ErrorLogger.addLogEntry(e.toString());
			return "";
		} catch (UnsupportedEncodingException e) {
			ErrorLogger.addLogEntry(e.toString());
			return "";
		}
	}
	
	public static String getTenCharNameHash(String base) {
        StringBuffer buffer = new StringBuffer();
        String characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int charactersLength = characters.length();
        int saltLength = 10;

        for (int i = 1; i <= saltLength; i++) {
            double index = Math.random() * charactersLength;
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
	}
	
}
