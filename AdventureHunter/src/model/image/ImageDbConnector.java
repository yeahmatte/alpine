package model.image;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.database.DBConnector;
import resource.image.AbstractImage;
import utils.Converter;
import utils.ErrorLogger;
import utils.exceptions.ObjectNotCreatedException;
import utils.exceptions.ObjectNotFoundException;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.4
 * @version 0.1
 */
public class ImageDbConnector {
	//AbstractImage
	private final static String imageIdField 		= "ImageId";
	private final static String imageTitleField 	= "ImageTitle";
	private final static String imagePathField 		= "ImagePath";
	private final static String imageDateField		= "UploadTime";
	
	//ActivityImages
	private static final String dbActivityImageTable = DBConnector.getDbPrefix()+"ActivityImages";
	private final static String activityIdField	= "ActivityId";
	
	//ProfilePicture
	private static final String dbActivityTable = DBConnector.getDbPrefix()+"UserImages";
	private final static String userProfileImageField 	= "UserId";
	
	
	/**
	 * 
	 * @param imageId
	 * @return
	 */
	public static AbstractImage getActivityImage(Integer imageId) throws ObjectNotFoundException {
		String statement = String.format("SELECT %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT 0,1",
				imageIdField , imageTitleField , imagePathField, activityIdField, 
				imageDateField, dbActivityImageTable , imageIdField);
		
		List<String> inputValues = new ArrayList<String>(); 
		inputValues.add(imageId.toString());
		
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, inputValues);
			if(!rs.next())
				throw new ObjectNotFoundException("Image wasn't found in the database");
			return new ActivityImage(new Integer(rs.getInt(imageIdField)),
							rs.getString(imagePathField), rs.getString(imageTitleField),
							Converter.TimeStampToCalendar(rs.getTimestamp(imageDateField)),
							new Integer(rs.getInt(activityIdField)));
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotFoundException("Image wasn't found in the database");
		}catch(ObjectNotFoundException e2){
			ErrorLogger.addLogEntry(e2);
			throw e2;
		}
	}
	
	/**
	 * 
	 * @param activityId
	 * @return
	 */
	public static List<AbstractImage> getActivityImages(Integer activityId) {
		String statement = String.format("SELECT %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT 0,100",
				imageIdField , imageTitleField , imagePathField, activityIdField, 
				imageDateField, dbActivityImageTable , activityIdField);
		List<String> inputValues = new ArrayList<String>();
		inputValues.add(activityId.toString());
		
		List<AbstractImage> imageList = new ArrayList<AbstractImage>();
		
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, inputValues);
			while(rs.next()) {
				imageList.add(new ActivityImage(new Integer(rs.getInt(imageIdField)),
						rs.getString(imagePathField), rs.getString(imageTitleField),
						Converter.TimeStampToCalendar(rs.getTimestamp(imageDateField)),
						new Integer(rs.getInt(activityIdField))));
			}
			return imageList;	
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			return new ArrayList<AbstractImage>();
		}
	}
	
	/**
	 * 
	 * @param image
	 * @return
	 * @throws ObjectNotCreatedException
	 */
	public static int createActivityImage(ActivityImage image) throws ObjectNotCreatedException {
		String statement = String.format("Insert into %s(%s, %s, %s) VALUES(?,?,?)",
				dbActivityImageTable, imageTitleField, imagePathField, activityIdField);
		
		List<String> inputValues = new ArrayList<String>(); 
		inputValues.add(image.getTitle());
		inputValues.add(image.getPath());
		inputValues.add(""+image.getActivityId());
		
		try{
			return DBConnector.getInstance().executeUpdateStatement(statement, inputValues,true);
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotCreatedException(e.getMessage());
		}
	}
}
