package model.image;

import java.util.Calendar;

import resource.image.AbstractImage;

public class ActivityImage extends AbstractImage{
	private int activityId;
	
	/**
	 * Empty construct
	 */
	public ActivityImage() {
		super(null,null);
		activityId = 0;
	}
	
	/**
	 * 
	 * @param path
	 * @param title
	 */
	public ActivityImage(String path, String title) {
		super(path,title);
		activityId = 0;
	}
	
	/**
	 * 
	 * @param imageId
	 * @param path
	 * @param title
	 * @param uploadTime
	 * @param activityId
	 */
	public ActivityImage(Integer imageId, String path, String title, Calendar uploadTime ,int activityId) {
		super(imageId,path,title,uploadTime);
		this.activityId = activityId;
	}
	
	/**
	 * 
	 * @return returns a HTML tag for the image
	 */
	public String getHTMLtag() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 * @return returns the Id of the image
	 */
	public int getImageId() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Set the activity id for the image.
	 * @param activityId
	 */
	public void setActivtyId(int activityId) {
		this.activityId = activityId;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getActivityId() {
		return activityId;
	}
	
	/**
	 * 
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("Activity Id: " + activityId + System.getProperty("line.separator"));
		return sb.toString();
	}
}
