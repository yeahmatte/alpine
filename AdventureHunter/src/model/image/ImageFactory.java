package model.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import resource.system.ActivityImageSettingsEnum;
import utils.exceptions.ObjectNotCreatedException;

public class ImageFactory {
	
	public ImageFactory() {
		
	}
	
	private void saveImageFromStream(InputStream inputFile, String savePath, String fileName,int dstWidth, int dstHeight)  throws IOException {
		InputStream imageStream = inputFile;
		BufferedImage srcImage = ImageIO.read(imageStream);
		if (srcImage == null) { 
			System.err.println("NO SOURCE IMAGE!"); 
			throw new ObjectNotCreatedException("Null image");
		}
		
		savePath = "C:\\Users\\yeahmatte\\Documents\\git\\alpine\\AdventureHunter\\WebContent\\Images\\Activity\\";
		
		BufferedImage dstImage = new BufferedImage(dstWidth, dstHeight, BufferedImage.TYPE_INT_RGB);
		dstImage.getGraphics().drawImage(srcImage, 0, 0, dstWidth, dstHeight, null);
		ImageIO.write(dstImage, "jpg", new File(savePath + fileName + ""));
	}
	
	public void saveActivityImageFromStream(InputStream inputFile, String savePath, String fileName)  throws IOException {
		int dstWidth = ActivityImageSettingsEnum.IMAGE_WIDTH.getValue();
		int dstHeight = ActivityImageSettingsEnum.IMAGE_WIDTH.getValue();
		
		saveImageFromStream(inputFile,savePath,fileName,dstWidth,dstHeight);
	}
}
