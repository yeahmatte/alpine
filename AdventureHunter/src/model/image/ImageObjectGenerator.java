package model.image;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import resource.image.AbstractImage;
import resource.image.ImageGeneratorInterface;
import utils.ImageNameGenerator;
import utils.exceptions.ObjectNotCreatedException;

/**
 * 
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.4
 */
public class ImageObjectGenerator implements ImageGeneratorInterface {
	private final static String activityImagePath = "/Images/Activity/";

	/**
	 * 
	 */
	public AbstractImage getActivityImage(int imageId) {
		return ImageDbConnector.getActivityImage(imageId);
	}

	/**
	 * 
	 */
	public List<AbstractImage> getActivityImages(Integer activityId) {
		return ImageDbConnector.getActivityImages(activityId);
	}

	/**
	 * 
	 */
	public AbstractImage createNewActivityImage(int activityId, InputStream inputFile) {

		String savePath = activityImagePath;
		String fileName = "a"+activityId+"_"+ImageNameGenerator.getTenCharNameHash(Calendar.getInstance().toString())+".jpg";
		ActivityImage imageObject = new ActivityImage(savePath+fileName,"Image for activity "+activityId);
		ImageFactory imageFactory = new ImageFactory();
		imageObject.setActivtyId(activityId);

		try {
			imageFactory.saveActivityImageFromStream(inputFile, savePath, fileName);
			int imageId = ImageDbConnector.createActivityImage(imageObject);
			imageObject.setImageId(imageId);

			return imageObject;
		} catch ( IOException e) {
			throw new ObjectNotCreatedException("Image could not be stored");
		} 
	}

}
