package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.1
 */
public class DBConnector {

	protected static DBConnector instance = null;
	protected Connection conn = null;
	private final static String dbPrefix = "AH_";

	private String host = "192.168.1.103:3306/adventurehunter";
	private String user = "yeahmatte";
	private String password = "matte.sql";
	
	protected DBConnector() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		  	conn = DriverManager.getConnection("jdbc:mysql://" + host + "?user="
		    				+ user + "&password=" + password);
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Gets the DBConnector instance
	 * @return The instance
	 */
	public static synchronized DBConnector getInstance() {
		if (instance == null) {
			try {
				instance = new DBConnector();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}
	
	/**
	 * 
	 * @param preparedStatementString
	 * @param statementValues
	 * @return
	 * @throws SQLException
	 */
	private PreparedStatement getStatement(String preparedStatementString, List<String> statementValues) throws SQLException{
		PreparedStatement preparedStatement = conn.prepareStatement(preparedStatementString);
		for (int index = 1; index <= statementValues.size(); index++ )
			preparedStatement.setString(index, statementValues.get(index-1));
		//TODO: REMOVE AFTER TEST
		System.out.println(preparedStatement.toString());
		return preparedStatement;
	}
	
	/**
	 * 
	 * @param preparedStatementString
	 * @param statementValues
	 * @return
	 * @throws SQLException
	 */
	private PreparedStatement getStatementWithReturnKey(String preparedStatementString, List<String> statementValues) throws SQLException{
		PreparedStatement preparedStatement = conn.prepareStatement(preparedStatementString,Statement.RETURN_GENERATED_KEYS);
		for (int index = 1; index <= statementValues.size(); index++ )
			preparedStatement.setString(index, statementValues.get(index-1));
		//TODO: REMOVE AFTER TEST
		System.out.println(preparedStatement.toString());
		return preparedStatement;
	}
	
	/**
	 * 
	 * @param preparedStatementString
	 * @param statementValues
	 * @return
	 * @throws SQLException
	 */
	public int executeUpdateStatement(String preparedStatementString, List<String> statementValues, boolean getKey) throws SQLException{
		if(getKey)
			return getStatementWithReturnKey(preparedStatementString, statementValues).executeUpdate();
		else
			return getStatement(preparedStatementString, statementValues).executeUpdate();
	}

	/**
	 * 
	 * @param preparedStatementString
	 * @param statementValues
	 * @return
	 * @throws SQLException
	 */
	public ResultSet executeSelectStatement(String preparedStatementString, List<String> statementValues) throws SQLException{
		return getStatement(preparedStatementString, statementValues).executeQuery();
	}
	
	public PreparedStatement getPreparedStatement(String statement) throws SQLException {
		PreparedStatement preparedStatement = conn.prepareStatement(statement);
		return preparedStatement;
	}
		
	/**
	 * 
	 * @param stringToValidate
	 * @return Escaped string
	 */
	protected String mysqlEscapeString(String stringToValidate){
		return null;
	}
	
	public static String getDbPrefix() {
		return dbPrefix;
	}
}
