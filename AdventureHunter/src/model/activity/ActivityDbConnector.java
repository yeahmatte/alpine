package model.activity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.database.DBConnector;
import resource.activity.ActivityInterface;
import resource.activity.ActivityTypeGenerator;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.GpsPoint;
import utils.Converter;
import utils.ErrorLogger;
import utils.exceptions.ObjectNotCreatedException;
import utils.exceptions.ObjectNotFoundException;

/**
 * Database methods for the activities.
 * Handles all the database interaction for
 * activity related classes.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class ActivityDbConnector {
	private static final String dbActivityTable = DBConnector.getDbPrefix()+"Activities";
	private final static String activityUserIdField = "UserId";
	private final static String activityIdField 	= "ActivityId";
	private final static String activityNameField 	= "ActivityName";
	private final static String activityDescField	= "ActivityDescription";
	private final static String activityTypeField 	= "ActivityType";
	private final static String startTimeField 		= "StartTime";
	private final static String startLatField 		= "StartLatitude";
	private final static String startLonField 		= "StartLongitude";
	private final static String totalDistanceField 	= "TotalDistance";
	private final static String totalTimeField		= "TotalTime";

	private final static String dbGpsPointsTable 	= DBConnector.getDbPrefix()+"ActivityGpsPoints";
	private final static String pointBelongsToField = "ActivityId";
	private final static String pointLatitudeField 	= "Latitude";
	private final static String pointLongitudeField = "Longitude";
	private final static String pointElevationField = "Elevation";
	private final static String pointTimeField 		= "Time";

	private static Activity activityFromResult(ResultSet rs) throws SQLException {
		return new Activity(new Integer(rs.getInt(activityUserIdField)), new Integer(rs.getInt(activityIdField)), 
				rs.getString(activityNameField), rs.getString(activityDescField), 
				ActivityTypeGenerator.getInstance().getObjectFromString(rs.getString(activityTypeField)),
				Converter.DateToCalendar(rs.getDate(startTimeField)), rs.getDouble(startLatField),
				rs.getDouble(startLonField), rs.getDouble(totalDistanceField), rs.getDouble(totalTimeField));
	}
	
	/**
	 * Saves the activity to the database.
	 * All the information shall be validated before
	 * this method is called. No validation other than
	 * for MySQL escapes is done.
	 * @param activity The activity to be created
	 * @return True if the activity was added otherwise false
	 */
	public static ActivityInterface createActivity(ActivityInterface activity) throws ObjectNotCreatedException {
		String activityStatement = String.format("Insert into %s(%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES(?,?,?,?,?,?,?,?,?)",
				dbActivityTable, activityUserIdField, activityNameField, activityDescField, activityTypeField, startTimeField, 
				startLatField, startLonField, totalDistanceField, totalTimeField);

		List<String> inputActivityValues = new ArrayList<String>(); 
		inputActivityValues.add(""+activity.getUserId());
		inputActivityValues.add(activity.getName());
		inputActivityValues.add(activity.getDescription());
		inputActivityValues.add(activity.getActivityType().toDbString());
		inputActivityValues.add(Converter.CalendarToDbString(activity.getStartTime()));
		inputActivityValues.add(""+activity.getStartLatitude());
		inputActivityValues.add(""+activity.getStartLongitude());
		inputActivityValues.add(""+activity.getTotalDistance());
		inputActivityValues.add(""+activity.getTotalTime());

		try {
			int activityId =  DBConnector.getInstance().executeUpdateStatement(activityStatement, inputActivityValues,true);
			if( activityId < 1)
				throw new ObjectNotCreatedException("The activity could not be created");

			activity.setId(activityId);

			String pointStatement = String.format("Insert into %s(%s, %s, %s, %s, %s) VALUES(%s,?,?,?,?)",
					dbGpsPointsTable, pointBelongsToField, pointLatitudeField, pointLongitudeField, 
					pointElevationField, pointTimeField, activity.getActivityId());
			List<GpsPointInterface> points = activity.getGpsPoints();
			PreparedStatement preStatement;

			preStatement = DBConnector.getInstance().getPreparedStatement(pointStatement);

			for (int i = 0; i < points.size(); i++) {
				GpsPointInterface point = points.get(i);
				preStatement.setDouble(1, point.getLatitude());
				preStatement.setDouble(2, point.getLongitude());
				preStatement.setDouble(3, point.getElevation());
				preStatement.setString(4, Converter.CalendarToDbString(point.getTime()));
				preStatement.addBatch();
				if ((i + 1) % 1000 == 0) {
					preStatement.executeBatch();
				}
			}
			preStatement.executeBatch();

			return activity;
		} catch(SQLException e) {
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotCreatedException("The activity could not be created");
		}catch(ObjectNotCreatedException e2) {
			ErrorLogger.addLogEntry(e2);
			throw e2;
		}
	}

	/**
	 * Used for executing statements.
	 * @param statement sql query
	 * @param userValues input values
	 * @return Activity object
	 * @throws ObjectNotFoundException Throws when the object is not found in the database.
	 */
	private static Activity getActivityExecute(String statement, List<String> userValues) throws ObjectNotFoundException{
		//TODO: Maybe change to ActivityInterface
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, userValues);
			if(!rs.next())
				throw new ObjectNotFoundException("Activity wasn't found in the database");

			return activityFromResult(rs);

		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotFoundException("Activity wasn't found in the database");
		}catch(ObjectNotFoundException e2){
			ErrorLogger.addLogEntry(e2);
			throw e2;
		}
	}

	/**
	 * Gets an activity object from the database
	 * @param activityId The Id for the requested activity
	 * @param getPoints True if the gps points for the activity should be fetched
	 * 					otherwise false if the gps points should be left as null.
	 * @return The requested activity object from the database
	 */
	public static ActivityInterface getActivity(Integer activityId, boolean getPoints) {
		String statement = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT 0,1",
				activityUserIdField , activityIdField , activityNameField, activityDescField, activityTypeField , 
				startTimeField , startLatField, startLonField , totalDistanceField, totalTimeField, dbActivityTable, activityIdField);
		List<String> inputValues = new ArrayList<String>(); 
		inputValues.add(activityId.toString());
		Activity activity = getActivityExecute(statement,inputValues);

		if(getPoints) {
			String statementGpsPoints = String.format("SELECT %s, %s, %s, %s FROM %s WHERE %s = ?",
					pointLatitudeField, pointLongitudeField, pointElevationField, pointTimeField, 
					dbGpsPointsTable, pointBelongsToField, activity.getActivityId());
			List<String> pointValues = new ArrayList<String>(); 
			pointValues.add(activityId.toString());
			
			try {
				ResultSet rsPoints = DBConnector.getInstance().executeSelectStatement(statementGpsPoints, pointValues);
				List<GpsPointInterface> gpsPoints = new ArrayList<GpsPointInterface>();
				while(rsPoints.next()) {
					gpsPoints.add(new GpsPoint(rsPoints.getDouble(pointLatitudeField), rsPoints.getDouble(pointLongitudeField),
											   rsPoints.getDouble(pointElevationField), Converter.TimeStampToCalendar(rsPoints.getTimestamp(pointTimeField))));	
				}
				activity.setGpsPoints(gpsPoints);
			} catch (SQLException e) {
				ErrorLogger.addLogEntry(e);
			}
		}
		return activity;
	}
	
	
	public static List<ActivityInterface> getUserActivities(int userId, int startValue, int numberOfActivities) {
		String statement = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT %s,%s",
				activityUserIdField , activityIdField , activityNameField, activityDescField, activityTypeField , 
				startTimeField , startLatField, startLonField , totalDistanceField, totalTimeField, dbActivityTable, activityUserIdField,
				startValue, numberOfActivities);
		List<String> inputValues = new ArrayList<String>();
		inputValues.add(""+userId);
		
		List<ActivityInterface> activityList = new ArrayList<ActivityInterface>();
		
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, inputValues);
			while(rs.next()) {
				activityList.add(activityFromResult(rs));
			}
			return activityList;	
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			return new ArrayList<ActivityInterface>();
		}
	}
}
