package model.activity;

import model.activity.activitytype.Skiing;
import resource.activity.AbstractActivityType;
import resource.activity.ActivityTypeGeneratorInterface;

/**
 * Used to generate the activity type objects.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class ActivityTypeObjectGenerator implements ActivityTypeGeneratorInterface{

	@Override
	public AbstractActivityType getObjectFromString(String activityType) {
		return new Skiing();
	}

}
