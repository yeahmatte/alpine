package model.activity.activitytype;

import model.gpsanalyzer.analyzerContainers.DistanceContainer;
import model.gpsanalyzer.analyzerContainers.ElevationContainer;
import model.gpsanalyzer.analyzerContainers.MapPointContainer;
import model.gpsanalyzer.analyzerContainers.SpeedContainer;
import model.gpsanalyzer.analyzerContainers.TimeContainer;
import resource.activity.AbstractActivityType;

/**
 * Should be used to analyze Alpine ski activities.
 * 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class Skiing extends AbstractActivityType {

	public Skiing() {
		containers.add(new DistanceContainer());
		containers.add(new SpeedContainer());
		containers.add(new ElevationContainer());
		containers.add(new TimeContainer(5));
		containers.add(new MapPointContainer());
		dbName = "Skiing";
		typeName = "Skiing";
	}
	
}
