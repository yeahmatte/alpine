package model.activity.activitytype;

import model.gpsanalyzer.analyzerContainers.DistanceContainer;
import model.gpsanalyzer.analyzerContainers.TimeContainer;
import resource.activity.AbstractActivityType;

/**
 * Should be used for the inital analysis
 * of an activity when it is just uploaded to
 * the system.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class CreationAnalysis extends AbstractActivityType {

	public CreationAnalysis() {
		containers.add(new DistanceContainer());
		containers.add(new TimeContainer(0.4));
		typeName = "Creation Analysis";
		dbName = "Unset";
	}
}
