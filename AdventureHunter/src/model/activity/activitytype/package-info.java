/**
 * This package contains the implementation of
 * the diffrent activity types.
 * This package should only have dependencies out from it,
 * no other package except the model.activity should depend on this.
 */
/**
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
package model.activity.activitytype;