package model.activity;

import java.util.Calendar;
import java.util.List;

import model.gpsanalyzer.AbstractGpsAnalyzerContainer;
import resource.activity.AbstractActivityType;
import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;

/**
 * Represents an activity in the system.
 * Used to hold data and make caluclations.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class Activity implements ActivityInterface {
	private int userId;
	private int activityId;
	private String activityName;
	private String activityDescription;
	private AbstractActivityType activityType;
	private Calendar startTime;
	private double startLatitude;
	private double startLongitude;
	private double totalDistance;
	private double totalTime;
	private List<GpsPointInterface> gpsPoints;
	
	/**
	 * NUllconstruct
	 */
	public Activity() {
		
	}
	
	/**
	 * Standard construtor
	 * @param userId Userid for the user that performed the activity
	 * @param activityId Unique Id
	 * @param activityName Name for the activity
	 * @param activityDesc Description of the activity entered by the user
	 * @param activityType Type of activity
	 * @param startTime Calendor of the start time
	 * @param startLat Start latitude of the acitivty
	 * @param startLon start longitude of the activity
	 * @param totalDistance The total distance traveled in the activity.
	 * @param totalTime Total time elapsed during the activity.
	 */
	public Activity(int userId, int activityId , String activityName, String activityDesc, AbstractActivityType activityType,
					Calendar startTime, double startLat, double startLon, double totalDistance, double totalTime) {
		this.userId = userId;
		this.activityId = activityId;
		this.activityName = activityName;
		this.activityDescription = activityDesc;
		this.activityType = activityType;
		this.startTime = startTime;
		this.startLatitude = startLat;
		this.startLongitude = startLon;
		this.totalDistance = totalDistance;
		this.totalTime = totalTime;
	}
	
	/**
	 * Returns the name of the activity
	 *  @return Name
	 */
	public String getName() {
		return activityName;
	}

	/**
	 * Returns the Activity type
	 * @return the set activity type
	 */
	public AbstractActivityType getActivityType() {
		return activityType;
	}

	/**
	 * Returns the name of the activity type.
	 * The name is meant to be used as the display name
	 * for a activity type.
	 * @return activity type name
	 */
	public String getActivityTypeName() {
		return activityType.getTypeName();
	}

	/**
	 * 
	 */
	public String getDescription() {
		return activityDescription;
	}

	/**
	 * 
	 */
	public Calendar getStartTime() {
		return startTime;
	}

	@Override
	public double getTotalElevationGained() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTotalElevationLost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMinElevation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMaxElevation() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTotalElapsedTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTotalRestTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTotalActiveTime() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getAverageSpeed() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMaxSpeed() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getMaxAcceleration() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getTotalDistance() {
		return totalDistance;
	}

	public double getTotalTime() {
		return totalTime;
	}
	
	
	/**
	 * 
	 */
	public void setName(String name) {
		this.activityName = name;
		
	}

	/**
	 * 
	 */
	public void setDescription(String description) {
		this.activityDescription = description;
		
	}

	/**
	 * 
	 */
	public void setActivityType(AbstractActivityType activityType) {
		this.activityType = activityType;
		
	}

	/**
	 * 
	 */
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
		
	}

	/**
	 * 
	 */
	public void setStartCoords(double lat, double lon) {
		this.startLatitude = lat;
		this.startLongitude = lon;
	}

	/**
	 * 
	 */
	public void setGpsPoints(List<GpsPointInterface> gpsPoints) {
		this.gpsPoints = gpsPoints;
	}

	/**
	 * 
	 */
	public int getActivityId() {
		return activityId;
	}

	@Override
	public void saveActivity() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 
	 */
	public double getStartLatitude() {
		return startLatitude;
	}

	/**
	 * 
	 */
	public double getStartLongitude() {
		return startLongitude;
	}

	/**
	 * 
	 */
	public List<GpsPointInterface> getGpsPoints() {
		return gpsPoints;
	}

	/**
	 * 
	 */
	public void setId(int activityId) {
		this.activityId = activityId;
	}

	@Override
	public void setUserId(int userId) {
		this.userId = userId;
		
	}

	@Override
	public Integer getUserId() {
		return userId;
	}

	/**
	 * Used for test
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("UserId: "+ userId+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("ActivityId: "+ activityId+"\n");
		
		sb.append(System.getProperty("line.separator"));
		
		sb.append("activtyName: "+ activityName+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Activity description: "+ activityDescription+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Activity Type: "+ activityType.getTypeName()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Start Time: "+ startTime+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Start Latitude: "+ startLatitude+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Start Longitude: "+ startLongitude+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Total Distance: "+ totalDistance+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("No. of Gps points: "+ gpsPoints.size()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		return sb.toString();
	}

	/**
	 * 
	 */
	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}

	/**
	 * 
	 */
	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	@Override
	public AbstractGpsAnalyzerContainer getContainer(String containerName) {
		return activityType.getAnalyzerContainer(containerName);
	}

	@Override
	public boolean hasContainer(String containerName) {
		return activityType.hasAnalyzerContainer(containerName);
	}
	
}
