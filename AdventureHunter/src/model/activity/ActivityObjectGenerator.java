/**
 * 
 */
package model.activity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import model.gpsanalyzer.GpsLogAnalyzer;
import model.gpsanalyzer.GpsPoint;
import model.gpsanalyzer.utils.GpxReader;
import model.gpsanalyzer.utils.topografix.gpx._1._1.GpxType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.TrkType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.TrksegType;
import model.gpsanalyzer.utils.topografix.gpx._1._1.WptType;
import resource.activity.ActivityGeneratorInterface;
import resource.activity.ActivityInterface;
import resource.activity.ActivityTypeGenerator;
import resource.activity.GpsPointInterface;
import resource.user.AbstractUser;
import utils.exceptions.ObjectNotCreatedException;
import utils.exceptions.ObjectNotFoundException;

/**
 * Object generator for activities.
 * The class is meant to construct all activity objects
 * for the servlets or other classes that needs an activity object.
 * Only this class has a connection to the activity model used.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class ActivityObjectGenerator implements ActivityGeneratorInterface {
	private final static String defaultName = "Untitled";
	private final static String defaultActivityType = "CreationAnalysis";
	
	
	/**
	 * Standard constructor
	 */
	public ActivityObjectGenerator() {

	}

	/**
	 * Constructs a new activity from an stream provided by the
	 * user. This method also saves the new activity in the database.
	 * @param inputFile The stream with the gpx file.
	 * @param user The user that have uploaded the activity
	 * @return The newly created activity object
	 * @throws ObjectNotCreatedException If the activity could not be created
	 */
	public ActivityInterface createNewActivity(InputStream inputFile, AbstractUser user) throws ObjectNotCreatedException {
		List<GpsPointInterface> gpsPoints = new ArrayList<GpsPointInterface>();
		
		//Parses the file to java object
		GpxReader gpxReader = new GpxReader();
		GpxType gpxObject = gpxReader.readGpx(inputFile);

		List<TrkType> gpxTrk = gpxObject.getTrk();
		TrkType gpxTrk1 = (gpxTrk.size() > 0) ? gpxTrk.get(0) : null;
		List<TrksegType> trkseg = (gpxTrk1 != null) ? gpxTrk1.getTrkseg() : null;
		TrksegType gpxTrkseg1 = (trkseg != null && trkseg.size() > 0) ? trkseg.get(0) : null;
		List<WptType> gpxTrkSeg1WPTs = gpxTrkseg1.getTrkpt();

		//Converts waypoints to GpsPoints
		for(WptType wpt : gpxTrkSeg1WPTs){
			gpsPoints.add(new GpsPoint(wpt.getLon().doubleValue(), wpt.getLat().doubleValue(),
					wpt.getEle().doubleValue(),wpt.getTime().toGregorianCalendar()));
		}

		ActivityInterface activity = new Activity();
		
		String activityName = gpxTrk1.getName();
		if(activityName == null || activityName.compareTo("")==0)
			activityName = defaultName;
		
		//Set basic values
		activity.setUserId(user.getUserId());
		activity.setName( activityName );
		activity.setDescription(gpxTrk1.getDesc());
		activity.setActivityType(ActivityTypeGenerator.getInstance().getObjectFromString(defaultActivityType));
		activity.setStartCoords(gpsPoints.get(0).getLatitude(), gpsPoints.get(0).getLongitude());
		activity.setStartTime(gpsPoints.get(0).getTime());
		activity.setGpsPoints(gpsPoints);

		// Add total time and distance
		GpsLogAnalyzer logAnalyzer = new GpsLogAnalyzer();
		logAnalyzer.setGpsPoints(activity.getGpsPoints());
		logAnalyzer.setActivityType(activity.getActivityType());
		logAnalyzer.analyze();
		logAnalyzer.setValuesToActivity(activity);
		
		activity = ActivityDbConnector.createActivity(activity);

		return activity;
	}

	/**
	 * Fetches a activity from the database and
	 * creates a activity object from the collected
	 * information. This method should always be used
	 * for fetching activities from the database.
	 * @param activityId Id of the requeste activity.
	 * @param getGpsPoints True if the gps points associated
	 * 				with the acitivity also should be fetched.
	 */
	public ActivityInterface getActivity(int activityId, boolean getGpsPoints) {
		try {
			return ActivityDbConnector.getActivity(activityId, getGpsPoints);
		} catch (ObjectNotFoundException e) {
			return null;
		}
	}

	@Override
	public List<ActivityInterface> getUserLatestActivities(int userId) {
		try {
			return ActivityDbConnector.getUserActivities(userId,0,10);
		} catch (ObjectNotFoundException e) {
			return new ArrayList<ActivityInterface>();
		}
	}

}
