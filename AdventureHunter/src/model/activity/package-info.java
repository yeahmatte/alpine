/**
 * This package contains the implemenation of the activities.
 * This package should only have dependencies out from it,
 * no other package should be dependent on this.
 */
/**
 * @author Mattias Mellhorn
 * @since 0.2
 *
 */
package model.activity;