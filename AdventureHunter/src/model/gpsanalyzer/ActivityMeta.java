package model.gpsanalyzer;

import java.util.Calendar;

public class ActivityMeta {
	private String activityName;
	private String activityDesc;
	private double startLat;
	private double startLon;
	private Calendar startTime;
	
	public ActivityMeta(String activityName, String activityDesc, double startLat, double startLon, Calendar startTime) {
		this.activityName = activityName;
		this.activityDesc = activityDesc;
		this.startLat = startLat;
		this.startLon = startLon;
		this.startTime = startTime;
	}
	
	public String getActivityName() {
		return activityName;
	}
	
	public String getActivityDesc() {
		return activityDesc;
	}
	
	public double getStartLongitude() {
		return startLon;
	}
	
	public double getStartLatitude() {
		return startLat;
	}
	
	public Calendar getStartTime() {
		return startTime;
	}
	
}
