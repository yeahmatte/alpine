package model.gpsanalyzer;

import java.util.ArrayList;
import java.util.List;

import resource.activity.AbstractActivityType;
import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import utils.exceptions.AnalysisFailedException;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class GpsLogAnalyzer {
	private List<GpsPointInterface> gpsPoints;
	private AbstractActivityType activityType; 
	private List<AbstractGpsAnalyzerContainer> analyzerContainers = new ArrayList<AbstractGpsAnalyzerContainer>();
	
	
	/**
	 * Standard construct
	 */
	public GpsLogAnalyzer() {
		this.gpsPoints = null;
		this.activityType = null;
	}
	
	public GpsLogAnalyzer(List<GpsPointInterface> gpsPoints) {
		this.gpsPoints = gpsPoints;
		this.activityType = null;
	}
	
	/**
	 * Set the gps points to analyze
	 * @param gpsPoints
	 */
	public void setGpsPoints(List<GpsPointInterface> gpsPoints) {
		this.gpsPoints = gpsPoints;
	}
	
	/**
	 * Sets the activity type for analysis
	 * @param activityType the activity type to use in the analysis
	 */
	public void setActivityType(AbstractActivityType activityType) {
		this.activityType = activityType;
		analyzerContainers = activityType.getAnalyzerContainers();
	}
	
	/**
	 * Starts the analysis
	 */
	public void analyze() throws AnalysisFailedException {
		if(gpsPoints != null && gpsPoints.size() > 0 && activityType != null)
			analyzePoints();
		else
			throw new AnalysisFailedException("No points to analyze");
		
	}
	
	private void analyzePoints() {
		for(AbstractGpsAnalyzerContainer container : analyzerContainers) {
			container.setNumberOfPoints(gpsPoints.size());
		}
		int pointNbr = 0;
		for(GpsPointInterface point : gpsPoints) {
			for(AbstractGpsAnalyzerContainer container : analyzerContainers) {
				container.addPoint(point,pointNbr);
			}
			pointNbr++;
		}
		//TODO: REMOVE AFTER TEST
		System.out.println("NbrOfPoints: "+gpsPoints.size());
	}
	
	public String getAnlysisResult() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.getProperty("line.separator"));
		sb.append(System.getProperty("line.separator"));
		
		for(AbstractGpsAnalyzerContainer container : analyzerContainers) {
			sb.append(container.toString());
			sb.append(System.getProperty("line.separator"));
			sb.append(System.getProperty("line.separator"));
		}
		
		return sb.toString();
	}
	
	public void setValuesToActivity(ActivityInterface activity){
		for(AbstractGpsAnalyzerContainer container : analyzerContainers) {
			container.setValuesToActivity(activity);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public List<GpsPointInterface> getGpsPoints() {
		return gpsPoints;
	}
}
