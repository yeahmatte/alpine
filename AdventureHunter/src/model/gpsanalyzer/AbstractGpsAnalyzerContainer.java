package model.gpsanalyzer;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;

public abstract class AbstractGpsAnalyzerContainer {
	protected GpsPointInterface lastPoint;
	protected int numberOfPoints;
	protected String containerName;
	protected StringBuilder graphLabels;
	protected int nbrOfGraphLabels;
	
	protected AbstractGpsAnalyzerContainer() {
		lastPoint = null;
	}
	
	public void addPoint(GpsPointInterface point, int pointNbr) {
		lastPoint = point;
	}
	
	public void setNumberOfPoints(int nbrOfPoints) {
		this.numberOfPoints = nbrOfPoints;
	}
	
	public abstract void setValuesToActivity(ActivityInterface activity);
	
	public abstract boolean isEmpty();

	public String getContainerName() {
		return containerName;
	}
	
	public String getGraphLabels() {
		if ( graphLabels.length() > 0 && graphLabels.charAt(graphLabels.length() - 1) == ',' )
			return graphLabels.deleteCharAt(graphLabels.length()-1).toString();
		else if ( graphLabels.length() > 0)
			return graphLabels.toString();
		else
			return "";
	}
}
