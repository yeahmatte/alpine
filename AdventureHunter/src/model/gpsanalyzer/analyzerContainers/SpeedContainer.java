package model.gpsanalyzer.analyzerContainers;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Should be used to keep track of the speed data
 * when analyzing a GPS log file.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class SpeedContainer extends AbstractGpsAnalyzerContainer {
	private double maxSpeed;
	private double maxAcceleration;
	private double maxDescelartion;
	private Double lastSpeed;
	
	public SpeedContainer() {
		super();
		this.maxSpeed = 0;
		this.maxAcceleration = 0;
		this.maxDescelartion = 0;
		this.lastSpeed = null;
		containerName = "Speed";
		nbrOfGraphLabels = 200;
		graphLabels = new StringBuilder();
	}
	
	/**
	 * Analyze the speed/acceleration data compared with
	 * the last point given to the object.
	 * @param point the point to measure against
	 */
	public void addPoint(GpsPointInterface point, int pointNbr) {
		if(lastPoint != null) {
			//Speed
			double speed = lastPoint.speedFromPoint(point);
			if(speed > maxSpeed)
				maxSpeed = speed;
			
			//Acceleration
			if(lastSpeed != null) {
				double acceleration = ( ( (speed - lastSpeed)*(1000/(60*60)) ) / (lastPoint.timeBetweenPoint(point)/1000) );
				if ( acceleration > 0 && acceleration > maxAcceleration )
					maxAcceleration = acceleration;
				else if ( Math.abs(acceleration) > maxDescelartion)
					maxDescelartion = Math.abs(acceleration);
			}
			
			if((pointNbr % (numberOfPoints/nbrOfGraphLabels)) == 0)
				graphLabels.append(speed+",");
			
			//Set last speed
			lastSpeed = speed;
		}
		super.addPoint(point,pointNbr);
	}

	/**
	 * Returns the max speed in kph
	 * @return max speed in kph
	 */
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public double getMaxAcceleration() {
		return maxAcceleration;
	}
	
	public double getMaxDesceleration() {
		return maxDescelartion;
	}
	
	
	
	/**
	 * Used for testing
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("--- SpeedContainer ---");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Max Speed: "+ getMaxSpeed()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Max Acceleration: "+ getMaxAcceleration()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Max Desceleration: "+ getMaxDesceleration()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		return sb.toString();
	}

	/**
	 * 
	 */
	public void setValuesToActivity(ActivityInterface activity) {
		//Do nothing
	}

	@Override
	public boolean isEmpty() {
		return false;
	}
}
