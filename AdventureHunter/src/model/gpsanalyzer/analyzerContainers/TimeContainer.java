package model.gpsanalyzer.analyzerContainers;

import java.util.Calendar;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Should be used to keep track of the time data
 * when analyzing a GPS log file.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class TimeContainer extends AbstractGpsAnalyzerContainer {
	private double totalTime;
	private double totalTimeInMotion;
	private double restSpeed;
	private Calendar firstTime;

	
	public TimeContainer(double restSpeed) {
		super();
		this.totalTime = 0;
		this.totalTimeInMotion = 0;
		this.restSpeed = restSpeed;
		this.firstTime = null;
		containerName = "Time";
		nbrOfGraphLabels = 200;
		graphLabels = new StringBuilder();
	}

	/**
	 * Analyze the time data compared with
	 * the last point given to the object.
	 * @param point the point to measure against
	 */
	public void addPoint(GpsPointInterface point, int pointNbr) {
		double time;
		if(lastPoint != null) {
			time = lastPoint.timeBetweenPoint(point);
			
			totalTime += time;
			
			if(lastPoint.speedFromPoint(point) > restSpeed)
				totalTimeInMotion += time;
		}
		
		//Used for elapsed time
		if(firstTime == null ) {
			firstTime = point.getTime();
		}
		
		if ((pointNbr % (numberOfPoints/(nbrOfGraphLabels/10))) == 0) {
			graphLabels.append("\""+doubleToTime(totalTime)+"\",");
		} else if ((pointNbr % (numberOfPoints/nbrOfGraphLabels)) == 0) {
			graphLabels.append("\"\",");
		}
		
		super.addPoint(point , pointNbr);
	}

	/**
	 * returns the total time
	 * @return total time in milliseconds
	 */
	public double getTotalTime() {
		return totalTime;
	}

	/**
	 * Return the total time in motion.
	 * (Time spent above rest speed)
	 * @return time in motion in milliseconds
	 */
	public double getTotalTimeInMotion() {
		return totalTimeInMotion;
	}

	/**
	 * Return the total time in rest.
	 * (Time spent below rest speed)
	 * @return time in rest in milliseconds
	 */
	public double getTotalTimeInRest() {
		return (totalTime - totalTimeInMotion);
	}

	/**
	 * Maybe useful
	 * @return
	 */
	public double getTotalElapsedTime() {
		if(lastPoint != null && firstTime != null)
			return (lastPoint.getTime().getTimeInMillis() - firstTime.getTimeInMillis());
		return 0;
	}

	/**
	 * Used for testing
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("--- TimeContainer ---");
		sb.append(System.getProperty("line.separator"));

		sb.append("Total time: "+ doubleToTime(getTotalTime())+"\n");
		sb.append(System.getProperty("line.separator"));

		sb.append("Total time in motion: "+ doubleToTime(getTotalTimeInMotion())+"\n");
		sb.append(System.getProperty("line.separator"));

		sb.append("Total time in rest: "+ doubleToTime(getTotalTimeInRest())+"\n");
		sb.append(System.getProperty("line.separator"));

		sb.append("Total elapsed time: "+ doubleToTime(getTotalElapsedTime())+"\n");
		sb.append(System.getProperty("line.separator"));

		return sb.toString();
	}

	/**
	 * This gives rounding error of max 2 seconds ATM
	 * @param time the time in ms to convert to HH:mm:ss
	 * @return String formated as HH:mm:ss
	 */
	private String doubleToTime(double time) {
		long seconds = (long) time / 1000;
		long h = seconds / 3600;
		long remainder = seconds % 3600;
		long m = remainder / 60;
		long s = remainder % 60;

		return String.format("%d:%02d:%02d", h, m, s);
	}

	/**
	 * 
	 */
	public void setValuesToActivity(ActivityInterface activity) {
		activity.setTotalTime(getTotalTime());
	}

	@Override
	public boolean isEmpty() {
		return false;
	}
}
