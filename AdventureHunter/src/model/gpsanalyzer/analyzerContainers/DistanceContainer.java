package model.gpsanalyzer.analyzerContainers;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Should be used to keep track of the total distance
 * when analyzing a GPS log file.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class DistanceContainer extends AbstractGpsAnalyzerContainer {
	private double totalDistance;
	
	/**
	 * Constructor
	 * Nothing fancy here
	 */
	public DistanceContainer() {
		super();
		totalDistance = 0;
		containerName = "Distance";
		nbrOfGraphLabels = 200;
		graphLabels = new StringBuilder();
	}
	
	/**
	 * Adds the distance between this point and
	 * the last point passed to the object.
	 * @param point GpsPoint to measure distance to.
	 */
	public void addPoint(GpsPointInterface point, int pointNbr) {
		if( lastPoint != null )
		    totalDistance += lastPoint.distanceToPoint2D(point);
		
		if ((pointNbr % (numberOfPoints/(nbrOfGraphLabels/10))) == 0)
			graphLabels.append("\""+totalDistance+"\",");
		else if ((pointNbr % (numberOfPoints/nbrOfGraphLabels)) == 0)
			graphLabels.append("\"\",");
		
		super.addPoint(point,pointNbr);
	}
	
	/**
	 * Returns the total distance in meters
	 * @return total distance in meters
	 */
	public double getTotalDistance() {
		return totalDistance;
	}
	
	/**
	 * Used for testing
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("--- DistanceContainer ---");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Total Distance: "+ getTotalDistance()+"\n");
		sb.append(System.getProperty("line.separator"));
	
		return sb.toString();
	}

	@Override
	public void setValuesToActivity(ActivityInterface activity) {
		activity.setTotalDistance(getTotalDistance());
	}

	@Override
	public boolean isEmpty() {
		return false;
	}
}
