package model.gpsanalyzer.analyzerContainers;

import resource.activity.ActivityInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

public class EmptyContainer extends AbstractGpsAnalyzerContainer{

	public EmptyContainer() {
		super();
		containerName = "Empty"; 
	}
	
	@Override
	public void setValuesToActivity(ActivityInterface activity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isEmpty() {
		return true;
	}
	
}
