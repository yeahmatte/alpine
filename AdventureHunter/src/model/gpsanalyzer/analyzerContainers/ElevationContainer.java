package model.gpsanalyzer.analyzerContainers;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Should be used to keep track of the elevation data
 * when analyzing a GPS log file.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class ElevationContainer extends AbstractGpsAnalyzerContainer {
	private double maxElevation;
	private double minElevation;
	private double totalEleAsc;
	private double totalEleDesc;
	private double steepestAsc;
	private double steepestDesc;
	
	/**
	 * Standard constructor
	 */
	public ElevationContainer() {
		super();
		this.maxElevation = 0;
		this.minElevation = 0;
		this.totalEleAsc = 0;
		this.totalEleDesc = 0;
		this.steepestAsc = 0;
		this.steepestDesc = 0;
		containerName = "Elevation";
		nbrOfGraphLabels = 200;
		graphLabels = new StringBuilder();
	}
	
	
	/**
	 * Analyze the elevation data compared with
	 * the last point given to the object.
	 * @param point the point to measure against
	 */
	public void addPoint(GpsPointInterface point, int pointNbr) {
		if(lastPoint != null) {
			double elevation = point.getElevation();
			
			//Max and Min
			if(elevation > maxElevation)
				maxElevation = elevation;
			
			if(elevation < minElevation)
				minElevation = elevation;
			
			//Elevation diff
			double elevationDiff = lastPoint.elevationDifference(point);
			if(elevationDiff>0){
				totalEleAsc += Math.abs(elevationDiff);
			} else {
				totalEleDesc += Math.abs(elevationDiff);
			}
			
			// Gradient
			double gradient = Math.atan( (elevationDiff / lastPoint.distanceToPoint2D(point) )); 
			if(gradient > 0 && gradient > steepestAsc){
				steepestAsc = gradient;
			}else if( Math.abs(gradient) > steepestDesc) {
				steepestDesc = Math.abs(gradient);
			}
			
			if((pointNbr % (numberOfPoints/nbrOfGraphLabels)) == 0)
				graphLabels.append(elevation+",");
			
		} else {
			this.maxElevation = point.getElevation();
			this.minElevation = point.getElevation();
		}
		super.addPoint(point,pointNbr);
	}
	
	/**
	 * Returns the max elevation in meters above sea level.
	 * @return max elevation in meters
	 */
	public double getMaxElevation() {
		return maxElevation;
	}
	
	/**
	 * Returns the min elevation in meters above sea level.
	 * @return min elevation in meters
	 */
	public double getMinElevation() {
		return minElevation;
	}
	
	/**
	 * Returns the total meters of elevation ascended.
	 * @return total elevation ascended in meters
	 */
	public double getTotalElevationAscended() {
		return totalEleAsc;
	}
	
	/**
	 * Return the total meters of elevation descended.
	 * @return total elevation descended in meters
	 */
	public double getTotalElevationDescended() {
		return totalEleDesc;
	}
	
	/**
	 * Returns the steepest ascent gradient.
	 * @return The steepest ascent gradient in degrees
	 */
	public double getSteepestAscent() {
		return steepestAsc;
	}
	
	/**
	 * Returns the steepest descent gradient.
	 * @return The steepest descent gradient in degrees
	 */
	public double getSteepestDescent() {
		return steepestDesc;
	}
	
	/**
	 * Used for testing
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("--- ElevationContainer ---");
		sb.append(System.getProperty("line.separator"));

		sb.append("Max Elevation: "+ getMaxElevation()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Min Elevation: "+ getMinElevation()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Total Elevation Ascended: "+ getTotalElevationAscended()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Total Elevation Descended: "+ getTotalElevationDescended()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Steepest Descended: "+ getSteepestDescent()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		sb.append("Steepest Ascent: "+ getSteepestAscent()+"\n");
		sb.append(System.getProperty("line.separator"));
		
		return sb.toString();
	}


	/**
	 * 
	 */
	public void setValuesToActivity(ActivityInterface activity) {
		//Do nothing
	}


	@Override
	public boolean isEmpty() {
		return false;
	}
}
