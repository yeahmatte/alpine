package model.gpsanalyzer.analyzerContainers;

import resource.activity.ActivityInterface;
import resource.activity.GpsPointInterface;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Should be used to keep track of the elevation data
 * when analyzing a GPS log file.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class MapPointContainer extends AbstractGpsAnalyzerContainer {
	
	/**
	 * Standard constructor
	 */
	public MapPointContainer() {
		super();
		containerName = "Map";
		nbrOfGraphLabels = 200;
		graphLabels = new StringBuilder();
	}
	
	
	/**
	 * Analyze the elevation data compared with
	 * the last point given to the object.
	 * @param point the point to measure against
	 */
	public void addPoint(GpsPointInterface point, int pointNbr) {
		graphLabels.append("["+point.getLatitude()+","+point.getLongitude()+"],");
	}
	
	
	
	/**
	 * 
	 */
	public void setValuesToActivity(ActivityInterface activity) {
		//Do nothing
	}


	@Override
	public boolean isEmpty() {
		return false;
	}
}
