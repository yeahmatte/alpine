package model.gpsanalyzer;

import java.util.Calendar;

import resource.activity.GpsPointInterface;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public class GpsPoint implements GpsPointInterface {
	private double longitude;
	private double latitude;
	private double elevation;
	private Calendar time;
	
	public GpsPoint(double longitude, double latitude, double elevation, Calendar time) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.elevation = elevation;
		this.time = time;
	}
	
	/**
	 * Returns the longitude of the point
	 * @return longitude of the point
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Returns the latitude of the point
	 * @return Latitude of the point
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Returns the elevation of the point (meters)
	 * @return elevation of the point in meters above sea level
	 */
	public double getElevation() {
		return elevation;
	}

	/**
	 * Returns the timestamp of the point
	 * @return timestamp of the point
	 */
	public Calendar getTime() {
		return time;
	}

	/**
	 * The differance always assumes that the current point is the
	 * first one and caluclates the value accrodingly.
	 * This.elevation - point.elevation
	 * @return Difference in elevation
	 */
	public double elevationDifference(GpsPointInterface point) {
		return (elevation - point.getElevation());
	}


	/**
	 * Returns the distance between two gps points without
	 * taking elevation into account.
	 * The method assumes this.point comes before point.
	 * @return Distance between the to points in meters
	 */
	public double distanceToPoint2D(GpsPointInterface point) {
		double a = 6378137, b = 6356752.314245, f = 1 / 298.257223563;
		double L = Math.toRadians(point.getLongitude() - longitude);
		double U1 = Math.atan((1 - f) * Math.tan(Math.toRadians(latitude)));
		double U2 = Math.atan((1 - f) * Math.tan(Math.toRadians(point.getLatitude())));
		double sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
		double sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);
		double cosSqAlpha;
		double sinSigma;
		double cos2SigmaM;
		double cosSigma;
		double sigma;

		double lambda = L, lambdaP, iterLimit = 100;
		do 
		{
			double sinLambda = Math.sin(lambda), cosLambda = Math.cos(lambda);
			sinSigma = Math.sqrt(	(cosU2 * sinLambda)
									* (cosU2 * sinLambda)
									+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
									* (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
								);
			if (sinSigma == 0) 
			{
				return 0;
			}

			cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
			sigma = Math.atan2(sinSigma, cosSigma);
			double sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
			cosSqAlpha = 1 - sinAlpha * sinAlpha;
			cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;

			double C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
			lambdaP = lambda;
			lambda = 	L + (1 - C) * f * sinAlpha	
						* 	(sigma + C * sinSigma	
								* 	(cos2SigmaM + C * cosSigma
										* 	(-1 + 2 * cos2SigmaM * cos2SigmaM)
									)
							);
		
		} while (Math.abs(lambda - lambdaP) > 1e-12 && --iterLimit > 0);

		if (iterLimit == 0) 
		{
			return 0;
		}

		double uSq = cosSqAlpha * (a * a - b * b) / (b * b);
		double A = 1 + uSq / 16384
				* (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
		double B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
		double deltaSigma = 
					B * sinSigma
						* (cos2SigmaM + B / 4
							* (cosSigma 
								* (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM
									* (-3 + 4 * sinSigma * sinSigma)
										* (-3 + 4 * cos2SigmaM * cos2SigmaM)));
		
		return ( b * A * (sigma - deltaSigma) );
	}

	/**
	 * This distance calculation takes the elevation differance into account.
	 * The method assumes this.point is before point.
	 * @return The distance between the two points.
	 */
	public double distanceToPoint3D(GpsPointInterface point) {
		return Math.sqrt( Math.pow( distanceToPoint2D(point), 2 ) + Math.pow( elevationDifference(point), 2 ) );
	}


	/**
	 * Returns the speed the points in kph.
	 * The mehtod assumes that this.point is before point.
	 * @return speed between the to points in kph
	 */
	public double speedFromPoint(GpsPointInterface point) {
		return ( (distanceToPoint2D(point) / 1000) / (timeBetweenPoint(point) / (1000*60*60)) );
	}

	/**
	 * Returns the time difference in milliseconds.
	 * The method assumes that this.point is before point.
	 * point.time - this.time = return
	 * @return time difference in milliseconds
	 */
	public double timeBetweenPoint(GpsPointInterface point) {
		return new Double(point.getTime().getTimeInMillis() - this.time.getTimeInMillis());
	}

}
