package model.user;

import java.util.Calendar;

import resource.user.AbstractDisplayUser;
import resource.user.AbstractUserAccount;

/**
 * Implementation of the AbstractDisplayUser for the system
 * This represents an user profile within the system.
 * This is meant to be used when the system shall display user
 * information. For managing a user account the AbstractUserAccount
 * class shall be used.
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.1
 * @see AbstractUserAccount for reference
 */
public class DisplayUser extends AbstractDisplayUser{

    private String firstName;
    private String surname;
    private String email;
    private Calendar signUpDate;
    private Calendar lastLogin;
    
    /**
     * Constructor with all the params for all the attributes
     * in the account.
     * 
     * @param userName The user name
     * @param password The password of the user
     * @param firstName The first name of the user
     * @param surname The last name of the user
     * @param email The email address to contact the user
     * @param activeAccount Whether or not the account is active
     * @param userLevel Sets the access level within the system
     * @param salt The user's salt for password encryption
     * @param signUpDate the date when the user signed up for an account in the system
     * @param lasgLogin the date of the user's last login
     */
    public DisplayUser(Integer userId, String userName, String firstName, String surname, String email, Integer userLevel, Calendar signUpDate, Calendar lastLogin) {
    	
    	super(userId,userName,userLevel);
        if (firstName != null)
        this.firstName = firstName.trim();
        if(surname != null)
        this.surname = surname.trim();
        this.email = email.trim();
        this.signUpDate = signUpDate;
        this.lastLogin = lastLogin;
    }
    
    /**
     * Returns a string that contains the first name, belonging to the given
     * user account.
     * 
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns a string that contains the surname, belonging to the given user
     * account.
     * 
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Returns a string that contains the email, belonging to the given user
     * account.
     * 
     * @return the email
     */
    public String getEmailAddress() {
        return email;
    }

    /**
     * Returns a boolean value that is either true or false, depending on if the
     * user account is active or not.
     * 
     * @return true or false
     */
    public boolean isActive() {
        return (userLevel > 0);
    }

    /**
     * Returns the date when the user last signed in
     * @return Date when the user last signed in
     */
	public Calendar getLastLogin() {
		return lastLogin;
	}


    /**
     * Returns the date when the user signed up for an account in the system
     * @return Date when the user signed up for an account in the system
     */
	public Calendar getSignUpDate() {
		return signUpDate;
	}
	
	/**
	 * 
	 */
	public String toString() {
		return String.format("Username: %s, Userlevel: %d, Email: %s, last login: %s ",userName,userLevel,email,lastLogin.toString());
	}
}
