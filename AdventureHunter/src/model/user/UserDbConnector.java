package model.user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import resource.user.AbstractDisplayUser;
import resource.user.AbstractUserAccount;
import utils.Converter;
import utils.ErrorLogger;
import utils.exceptions.ObjectNotFoundException;
import model.database.DBConnector;

/**
 * This handles all the interaction with the storage subsystem.
 * This shall be the only point of interaction with the main storage connection
 * for the user model.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public class UserDbConnector{
	private final static String dbTable = DBConnector.getDbPrefix()+"Users";
	// All the fields in the table Users in the DB
	private final static String userIdField = "UserId";
	private final static String userNameField = "UserName";
	private final static String passwordField = "Password";
	private final static String firstNameField = "FirstName";
	private final static String surnameField = "Surname";
	private final static String emailField = "Email";
	private final static String userLevelField = "UserLevel";
	private final static String saltField = "Salt";
	private final static String signUpDateField = "SignUpDate";
	private final static String lastLoginField = "LastLogin";
	
	
	/**
	 * Updates the DB with the information currently
	 * in the User Account.
	 * All the information shall be validated before
	 * this method is called. No validation other than
	 * for MySQL escapes is done.
	 * @param userAccount The User account to update
	 * @return true if the account was updated otherwise false
	 */
	public static boolean updateUserAccount(UserAccount userAccount){
		String statement = String.format("Update %s set %s = ?,  %s = ?, %s = ?, %s = ?, %s = ?, %s = ? where %s = ?",
										 dbTable, passwordField, firstNameField, surnameField, emailField, userLevelField, 
										 saltField, userIdField);
		
		List<String> userValues = new ArrayList<String>(); 
		userValues.add(userAccount.getPassword());
		userValues.add(userAccount.getFirstName());
		userValues.add(userAccount.getSurname());
		userValues.add(userAccount.getEmailAddress());
		userValues.add(userAccount.getUserLevel().toString());
		userValues.add(userAccount.getSalt());
		//userValues.add(userAccount.getLastLogin().toString());
		userValues.add(userAccount.getUserId().toString());

		try{
			return (1==DBConnector.getInstance().executeUpdateStatement(statement, userValues,false));
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			return false;
		}
	}

	/**
	 * Saves a new user account in the DB with the information currently
	 * in the User Account.
	 * All the information shall be validated before
	 * this method is called. No validation other than
	 * for MySQL escapes is done.
	 * @param userAccount The User account to be created
	 * @return True if the user was added otherwise false
	 */
	public static boolean createUserAccount(AbstractUserAccount userAccount){
		String statement = String.format("Insert into %s(%s, %s, %s, %s, %s, %s, %s) VALUES(?,?,?,?,?,?,?)",
				 dbTable, userNameField, passwordField, firstNameField, surnameField, emailField, userLevelField, 
				 saltField);
		
		List<String> userValues = new ArrayList<String>(); 
		userValues.add(userAccount.getUserName());
		userValues.add(userAccount.getPassword());
		userValues.add(userAccount.getFirstName());
		userValues.add(userAccount.getSurname());
		userValues.add(userAccount.getEmailAddress());
		userValues.add(userAccount.getUserLevel().toString());
		userValues.add(userAccount.getSalt());
		
		try{
			return (1==DBConnector.getInstance().executeUpdateStatement(statement, userValues,false));
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			return false;
		}
	}	

	/**
	 * 
	 * @param statement
	 * @param userValues
	 * @return
	 * @throws UserNotFoundException
	 */
	private static UserAccount getUserAccountExecute(String statement, List<String> userValues) throws ObjectNotFoundException{
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, userValues);
			if(!rs.next())
				throw new ObjectNotFoundException("User wasn't found in the database");
			
			return new UserAccount(new Integer(rs.getInt(userIdField)),rs.getString(userNameField), rs.getString(passwordField), 
								   rs.getString(firstNameField),rs.getString(surnameField), rs.getString(emailField), 
								   new Integer(rs.getInt(userLevelField)), rs.getString(saltField),  
								   Converter.DateToCalendar(rs.getDate(signUpDateField)), Converter.TimeStampToCalendar(rs.getTimestamp(lastLoginField)) );
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotFoundException("User wasn't found in the database");
		}catch(ObjectNotFoundException e2){
			ErrorLogger.addLogEntry(e2);
			throw e2;
		}
	}
	
	/**
	 * 
	 * @param userName user name of the account requested
	 * @return A SessionUser object with the user information
	 * @throws UserNotFoundException
	 */
	public static UserAccount getUserAccount(Integer userId) throws ObjectNotFoundException{
		String statement = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT 0,1",
				 userIdField, userNameField, passwordField, firstNameField, surnameField, userLevelField, 
				 emailField, saltField, signUpDateField, lastLoginField, dbTable, userIdField );
		List<String> userValues = new ArrayList<String>(); 
		userValues.add(userId.toString());
		return getUserAccountExecute(statement,userValues);
	}
	
	/**
	 * 
	 * @param userName user name of the account requested
	 * @return A SessionUser object with the user information
	 * @throws UserNotFoundException
	 */
	public static UserAccount getUserAccount(String userName) throws ObjectNotFoundException{
		String statement = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s WHERE %s = ? LIMIT 0,1",
				 userIdField, userNameField, passwordField, firstNameField, surnameField, userLevelField, 
				 emailField, saltField, signUpDateField, lastLoginField, dbTable, userNameField );
		List<String> userValues = new ArrayList<String>(); 
		userValues.add(userName);
		return getUserAccountExecute(statement,userValues);
	}
	
	/**
	 * 
	 * @param userName user name of the account requested
	 * @return A SessionUser object with the user information
	 * @throws UserNotFoundException
	 */
	public static SessionUser getSessionUserFromUserName(String userName) throws ObjectNotFoundException{
		String statement = String.format("SELECT %s,%s,%s,%s,%s,%s,%s FROM %s WHERE %s = ? LIMIT 0,1",
				 userIdField, userNameField, passwordField, firstNameField, userLevelField, saltField, 
				 signUpDateField, dbTable, userNameField );
		
		List<String> userValues = new ArrayList<String>(); 
		userValues.add(userName);

		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, userValues);
			if(!rs.next())
				throw new ObjectNotFoundException("User wasn't found in the database");
			return new SessionUser(new Integer(rs.getInt(userIdField)),rs.getString(userNameField),new Integer(rs.getInt(userLevelField)),
								   rs.getString(firstNameField), Converter.TimeStampToCalendar(rs.getTimestamp(signUpDateField)),rs.getString(passwordField),
								   rs.getString(saltField));
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			throw new ObjectNotFoundException("User wasn't found in the database");
		}catch(ObjectNotFoundException e2){
			//ErrorLogger.addLogEntry(e);
			throw e2;
		}
	}
	
	/**
	 * 
	 * @param start
	 * @param numberOfUsers
	 * @return
	 */
	public static List<AbstractDisplayUser> getUserList(int start, int numberOfUsers) {
		String statement = String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s FROM %s ORDER BY %s DESC LIMIT %d,%d",
				 userIdField, userNameField, firstNameField, surnameField, userLevelField, 
				 emailField, signUpDateField, lastLoginField, dbTable, userNameField, start, numberOfUsers);
		List<String> userValues = new ArrayList<String>(); 
		List<AbstractDisplayUser> userList = new ArrayList<AbstractDisplayUser>();
		try{
			ResultSet rs = DBConnector.getInstance().executeSelectStatement(statement, userValues);
			while(rs.next()) {
				userList.add(new DisplayUser(new Integer(rs.getInt(userIdField)),rs.getString(userNameField), 
								   rs.getString(firstNameField),rs.getString(surnameField), rs.getString(emailField), 
								   new Integer(rs.getInt(userLevelField)), Converter.TimeStampToCalendar(rs.getTimestamp(signUpDateField)), 
								   Converter.TimeStampToCalendar(rs.getTimestamp(lastLoginField))));
			}
			return userList;	
		}catch(SQLException e){
			ErrorLogger.addLogEntry(e);
			return new ArrayList<AbstractDisplayUser>();
		}
	}
	
}
