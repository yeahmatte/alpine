package model.user;

import java.util.Calendar;

import resource.user.AbstractSessionUser;

/**
 * Is meant to be used as a session user for someone
 * who uses the system but is not currently signed in.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 * @see resource.user.AbstractSessionUser
 */
public class SessionGuest extends AbstractSessionUser {
	private Calendar lastLoginDate;
	private Calendar lastPageRequest;
	
	protected SessionGuest(Integer userId, String userName, Integer userLevel) {
		super(0, userName, 0);
	}
	
	public SessionGuest() {
		super(0,"Guset",0);
		lastLoginDate = Calendar.getInstance();
		lastPageRequest = Calendar.getInstance();
	}

	@Override
	public String getFirstName() {
		// TODO Auto-generated method stub
		return userName;
	}

	@Override
	public Calendar getLastLogin() {
		// TODO Auto-generated method stub
		return lastLoginDate;
	}

	@Override
	public Calendar getSignUpDate() {
		// TODO Auto-generated method stub
		return lastLoginDate;
	}

	@Override
	public Calendar getLastPageRequest() {
		// TODO Auto-generated method stub
		return lastPageRequest;
	}

	@Override
	public void setLastPageRequest() {
		lastPageRequest = Calendar.getInstance();
		
	}

	@Override
	public void saveLastLogin() {
		//Nothing to do
	}

	@Override
	public boolean isGuest() {
		return true;
	}

}
