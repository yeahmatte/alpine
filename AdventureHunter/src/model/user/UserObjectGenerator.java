package model.user;


import java.util.List;

import resource.user.AbstractDisplayUser;
import resource.user.AbstractSessionUser;
import resource.user.AbstractUser;
import resource.user.AbstractUserAccount;
import resource.user.UserGeneratorInterface;
import utils.PasswordHasher;
import utils.exceptions.InactiveUserException;
import utils.exceptions.ObjectNotFoundException;
import utils.exceptions.PasswordDidnotMatchException;
import utils.exceptions.ObjectNotCreatedException;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.2
 */
public class UserObjectGenerator implements UserGeneratorInterface {

	public UserObjectGenerator() {
		
	}
	
	/**
	 * Get a UserAccount from a userId.
	 * If the userId isnt found in the system a
	 * UserNotFoundException is thrown
	 * @param userId The userId to get the account for
	 * @return	Returns the UserAccount asscosiated with the userId
	 * @throws UserNotFoundException If the userId isnt found
	 */
	public AbstractUserAccount getUserAccount(int userId) throws ObjectNotFoundException{
		try{
			return UserDbConnector.getUserAccount(userId);
		}catch(ObjectNotFoundException e){
			//TODO ErrorLogger.addLogEntry(e);
			throw e;
		}
	}
	
	/**
	 * Get a UserAccount from a userName.
	 * If the userId isnt found in the sytem a
	 * UserNotFoundException is thrown.
	 * @param userName The userName to get the account for
	 * @return	Returns the UserAccount asscosiated with the userName
	 * @throws UserNotFoundException If the userName isnt found
	 */
	public AbstractUserAccount getUserAccount(String userName) throws ObjectNotFoundException{
		try{
			return UserDbConnector.getUserAccount(userName);
		}catch(ObjectNotFoundException e){
			//TODO ErrorLogger.addLogEntry(e);
			throw e;
		}
	}

	public AbstractDisplayUser getDisplayUser(AbstractUser userObject){
		//TODO implement this
		return null;
	}

	public AbstractDisplayUser getDisplayUser(int userId){
		//TODO implement this
		return null;
	}

	public List<AbstractDisplayUser> getDisplayUsers(List<Integer> userIds){
		//TODO implement this
		return null;
	}
	
	/**
	 * Returns a list of display users ordered by user name.
	 * @param pageNo which page of 50 users to start at.
	 * @return A list that contains displayUsers.
	 */
	public List<AbstractDisplayUser> getUserList(int pageNo) {
		return UserDbConnector.getUserList(pageNo*50, 50);
	}

	/**
	 * Used to get a session user after a login attempt from a user.
	 * The method returns a AbstractSessionUser .
	 * @param userName Pattern validated User name from login
	 * @param password Pattern validated Password from login
	 * @return An SessionUser or if the user isn't found 
	 */
	public AbstractSessionUser getSessionUserFromLogin(String userName, String password) throws PasswordDidnotMatchException, ObjectNotFoundException, InactiveUserException {
		try{
			SessionUser user = UserDbConnector.getSessionUserFromUserName(userName);
			if(user.getUserLevel()<1)
				throw new InactiveUserException("This is not an active user");
			
			//TODO: Not good to have this check here. This logic shall be moved
			if(!PasswordHasher.hashPassword(password, user.getSalt()).equals(user.getPassword()))
				throw new PasswordDidnotMatchException("User password didn't match the one provided at login");
				
				//Updates the last login to now
				user.saveLastLogin();
				//To avoid the information being accessable in servlets
				user.clearPWandSalt();
				return user;
		}catch(ObjectNotFoundException | InactiveUserException | PasswordDidnotMatchException e){
			throw e;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public AbstractSessionUser getNewSessionGuest() {
		return new SessionGuest();
	}
	
	/**
	 * Creates a new UserAccount in the system
	 * and returns a AbstractUserAccount with the information.
	 * This method will add a new random salt to the account and
	 * assign a userId.
	 * @param userName The username of the new account
	 * @param password The password of the new account
	 * @param firstName The first name of the new account
	 * @param surname The surname of the new account
	 * @param email The email addrss of the new account
	 * @return A AbstractUserAccount with the information
	 * @throws UserNotFoundException If the user could not be saved to the system
	 * @throws ObjectNotCreatedException If the user could not be saved to the system
	 */
	public AbstractUserAccount createNewUserAccount(String userName, String password, String firstName, String surname, String email) throws ObjectNotFoundException, ObjectNotCreatedException{
		UserAccount newUser = new UserAccount(userName,firstName,surname,email);
		
		newUser.setSalt(PasswordHasher.getNewSalt());
		newUser.setPassword(password);
		newUser.setUserLevel(0);
		
		if(UserDbConnector.createUserAccount(newUser))
			 return getUserAccount(newUser.getUserName());
		else
			throw new ObjectNotCreatedException("The user could not be created");
	}
	
	/**
	 * Checks if a userName is registered in the system
	 * @param userName The userName to check for
	 * @return True if the username if the userName isnt found otherwise false
	 */
	public boolean isUserNameAvailable(String userName){
		try {
			getUserAccount(userName);
			return false;
		} catch(ObjectNotFoundException e){
			return true;
		}
	}
}