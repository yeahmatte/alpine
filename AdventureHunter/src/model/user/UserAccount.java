package model.user;

import java.util.Calendar;

import resource.user.AbstractUserAccount;
import utils.PasswordHasher;

/**
 * Implementation of the AbstractUserAccount for the system
 * This represents an user account within the system.
 * This is meant to be used when administrators or the user
 * self is managing account information
 * For display purposes use the AbstractDisplayUser
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.1
 * @see AbstractUserAccount for reference
 */
public class UserAccount extends AbstractUserAccount{

    private String firstName;
    private String surname;
    private String email;
    private String password;
    private String salt;
    private Calendar signUpDate;
    private Calendar lastLogin;
    
    /**
     * Constructor with all the params for all the attributes
     * in the account.
     * 
     * @param userName The user name
     * @param password The password of the user
     * @param firstName The first name of the user
     * @param surname The last name of the user
     * @param email The email address to contact the user
     * @param activeAccount Whether or not the account is active
     * @param userLevel Sets the access level within the system
     * @param salt The user's salt for password encryption
     * @param signUpDate the date when the user signed up for an account in the system
     * @param lasgLogin the date of the user's last login
     */
    public UserAccount(Integer userId, String userName, String password, String firstName, String surname, String email, Integer userLevel, String salt, Calendar signUpDate, Calendar lastLogin) {
    	
    	super(userId,userName,userLevel);
        this.password = password;
        if (firstName != null)
        this.firstName = firstName.trim();
        if(surname != null)
        this.surname = surname.trim();
        this.email = email.trim();
        this.salt = salt;
        this.signUpDate = signUpDate;
        this.lastLogin = lastLogin;
    }

    /**
     * The constructor to be used when creating a new account
     * @param userName
     * @param firstName
     * @param surname
     * @param email
     */
    public UserAccount(String userName, String firstName, String surname, String email) {
    	
    	super(0,userName,0);
        this.firstName = firstName.trim();
        this.surname = surname.trim();
        this.email = email.trim();
    }
    
    /**
     * Returns a string that contains the first name, belonging to the given
     * user account.
     * 
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returns a string that contains the surname, belonging to the given user
     * account.
     * 
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Returns a string that contains the salt, belonging to the given user
     * account.
     * 
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Returns a string that contains the password, belonging to the given user
     * account.
     * 
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns a string that contains the email, belonging to the given user
     * account.
     * 
     * @return the email
     */
    public String getEmailAddress() {
        return email;
    }

    /**
     * Returns a boolean value that is either true or false, depending on if the
     * user account is active or not.
     * 
     * @return true or false
     */
    public boolean isActive() {
        return (userLevel > 0);
    }

    /**
     * Returns the date when the user last signed in
     * @return Date when the user last signed in
     */
	public Calendar getLastLogin() {
		return lastLogin;
	}


    /**
     * Returns the date when the user signed up for an account in the system
     * @return Date when the user signed up for an account in the system
     */
	public Calendar getSignUpDate() {
		return signUpDate;
	}
	
    /**
     * Sets the first name for the user for the given user account, to the name
     * that is given as an argument.
     * 
     * @param firstname
     *            that is the name that the current first name should be set to
     */
    public void setFirstName(String firstname) {
        firstName = firstname;
    }

    /**
     * Sets the password for the user for the given user account, to the
     * password that is given as an argument.
     * 
     * @param password
     *            that is the password that the current password should be set
     *            to
     */
    public void setPassword(String password) {
        this.password = PasswordHasher.hashPassword(password, this.salt);
    }

    /**
     * Sets the surname for the user for the given user account, to the name
     * that is given as an argument.
     * 
     * @param surname
     *            that is the name that the current surname should be set to
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Sets the email for the user for the given user account, to the email that
     * is given as an argument.
     * 
     * @param email
     *            that is the email that the current email should be set to
     */
    public void setEmailAddress(String email) {
        this.email = email;
    }

    /**
     * Sets the user level for a specific user account 
     *
     * @param userLevel The new userlevel to set
     */
    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    /**
     * 
     * @return
     */
	public boolean saveUserAccount() {
		return UserDbConnector.updateUserAccount(this);
	}

    /**
     * Sets the salt for the user for the given user account
     * 
     * @param salt
     *            the new salt
     */
	public void setSalt(String salt) {
		this.salt = salt;
		
	}

	/**
	 * @param password
	 * @return 
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(PasswordHasher.hashPassword(password, this.salt));
	}
	
	/**
	 * 
	 */
	public String toString() {
		return String.format("Username: %s, Userlevel: %d, Email: %s, last login: %s ",userName,userLevel,email,lastLogin.toString());
	}

	@Override
	public void archive() {
		this.setUserLevel(-1);
		this.saveUserAccount();
	}
}
