package model.user;

import java.util.Calendar;

import resource.user.AbstractSessionUser;

/**
 * 
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 * @see AbstractSessionUser for reference on usage
 */
public class SessionUser extends AbstractSessionUser {
	protected String firstName;
	protected Calendar lastLogin;
	protected Calendar signUpDate;
	protected Calendar lastPageRequest;
	private String salt;
	private String password;
	
	public SessionUser(Integer userId, String userName, Integer userLevel,String firstName, Calendar SignUpDate) {
		super(userId, userName, userLevel);
		this.firstName = firstName;
		this.lastLogin = Calendar.getInstance();
		this.signUpDate = SignUpDate;
		this.lastPageRequest = Calendar.getInstance();
	}
	
	public SessionUser(Integer userId, String userName, Integer userLevel,String firstName, Calendar SignUpDate, String password, String salt) {
		super(userId, userName, userLevel);
		this.firstName = firstName;
		this.lastLogin = Calendar.getInstance();
		this.signUpDate = SignUpDate;
		this.lastPageRequest = Calendar.getInstance();
		this.password = password;
		this.salt = salt;
	}
	
	/**
	 * Returns a string that contains the first name, belonging to the given
	 * user account.
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Returns the date of the last login.
	 * 
	 * @return Date when the user last logged in
	 */
	public Calendar getLastLogin() {
		return lastLogin;
	}

	/**
	 * Returns the date when the user signed up for an account in the system.
	 * 
	 * @return Date when the user signed up for an account in the system
	 */
	public Calendar getSignUpDate() {
		return signUpDate;
	}

	/**
	 * Returns the date when the user last made a get request.
	 * 
	 * @return Date of the last get request made by the user
	 */
	public Calendar getLastPageRequest() {
		return lastPageRequest;
	}

	/**
	 * Set the last page request of the account to the current time.
	 */
	public void setLastPageRequest() {
		this.lastPageRequest = Calendar.getInstance();
	}

	/**
	 * Updates the database with the current LastLogin time.
	 */
	public void saveLastLogin() {
		
	}
	
	public String getSalt(){
		return salt;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void clearPWandSalt(){
		this.password = null;
		this.salt = null;
	}

	@Override
	public boolean isGuest() {
		// TODO Auto-generated method stub
		return false;
	}
}
