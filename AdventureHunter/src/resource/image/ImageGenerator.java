package resource.image;

import model.image.ImageObjectGenerator;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.4
 * @version 0.1
 */
public class ImageGenerator {
	private static ImageGeneratorInterface generator;
	
	public static ImageGeneratorInterface getInstance() {
		if(generator == null)
			generator = new ImageObjectGenerator();
		return generator;
	}
}
