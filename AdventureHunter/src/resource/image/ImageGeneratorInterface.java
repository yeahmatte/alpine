package resource.image;

import java.io.InputStream;
import java.util.List;

public interface ImageGeneratorInterface {

	//Activity Images
	public AbstractImage getActivityImage(int imageId);

	public List<AbstractImage> getActivityImages(Integer activityId);
	
	public AbstractImage createNewActivityImage(int activityId, InputStream inputFile);
	
	//User profile images
}
