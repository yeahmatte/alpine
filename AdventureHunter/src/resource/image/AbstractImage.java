package resource.image;

import java.util.Calendar;

/**
 * 
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.4
 */
public abstract class AbstractImage {
	protected String path;
	protected String title;
	protected Integer imageId;
	protected Calendar uploadTime;
	
	/**
	 * Standrad construct
	 * @param path relative path and filename 
	 * @param title Title or alt text for image
	 */
	protected AbstractImage(String path, String title) {
		this.path = path;
		this.title = title;
		this.imageId = null;
		this.uploadTime = null;
	}
	
	protected AbstractImage(Integer imageId, String path, String title) {
		this.path = path;
		this.title = title;
		this.imageId = imageId;
		this.uploadTime = null;
	}
	
	protected AbstractImage(Integer imageId, String path, String title, Calendar uploadTime) {
		this.path = path;
		this.title = title;
		this.imageId = imageId;
		this.uploadTime = uploadTime;
	}
	
	
	/**
	 * returns the path and filename of the image
	 * @return relative path and filename
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * Returns the title or alt the for the image
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Creates a tag that displays the image
	 * in an HTML tag.
	 * @return HTML tag.
	 */
	public abstract String getHTMLtag();
	
	/**
	 * Returns the id of the image.
	 * @return image id.
	 */
	public int getImageId() {
		return imageId; 
	}
	
	/**
	 * Sets the id of the image
	 * @param imageId the new id
	 */
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	/**
	 * Returns the time when the image was uploaded
	 * @return
	 */
	public Calendar getUploadTime() {
		return uploadTime;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Title: " + title + System.getProperty("line.separator"));
		sb.append("Path: " + path + System.getProperty("line.separator"));
		sb.append("Image Id: " + imageId + System.getProperty("line.separator"));
		sb.append("Time: " + uploadTime + System.getProperty("line.separator"));
		return sb.toString();
	}
	
}
