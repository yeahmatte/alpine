package resource.system;

public enum UserLevelEnum {
    BANNED(-2), ARCHIVED(-1), NOTACTIVATEDUSER(0), USER(1), MODERATOR(2), ADMINISTRATOR(3), SYSOP(5);

    private final int userLevel;
    UserLevelEnum(int userLevel) { 
    	this.userLevel = userLevel; 
    }
    
    public int getUserLevel() { 
    	return userLevel; 
    }
}
