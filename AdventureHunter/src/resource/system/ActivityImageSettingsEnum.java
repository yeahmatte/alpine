package resource.system;

public enum ActivityImageSettingsEnum {
    IMAGE_WIDTH(640), IMAGE_HEIGHT(480), IMAGE_MAX_SIZE(1024*1024*5);

    private final int settingValue;
    
    ActivityImageSettingsEnum(int settingValue) { 
    	this.settingValue = settingValue; 
    }
    
    public int getValue() { 
    	return settingValue; 
    }
}
