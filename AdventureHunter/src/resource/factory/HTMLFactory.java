package resource.factory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import resource.user.AbstractSessionUser;
import utils.ErrorLogger;

public class HTMLFactory {

	public static String getPageTitle() {
		return "Adventure Hunter";
	}

	public static String getHeadMeta() {
		return "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> \n" +
				"<meta name=\"Description\" CONTENT=\"Registration for www.adventurehunter.com,  Author: Adventure Hunter Inc.\"> \n";
	//			+ "<link href=\"/AdventureHunter/CSS/Menu/menu.css\" rel=\"stylesheet\" type=\"text/css\"> \n";
	}

	/**
	 * Creates a html link to the url with the display text of displayText
	 * @param url Url for the link
	 * @param displayText Text to display
	 * @return A HTML <a> tag in a string
	 */
	private static String getLink(String url, String displayText) {
		return String.format("<a href=\"%s\" >%s</a>",url,displayText);
	}

	public static String getLeftMenu() {
		//C:\\Users\\yeahmatte\\Documents\\git\\alpine\\AdventureHunter\\WebContent\\WEB-INF\\JSP\\Menu\\menu.html
		try {
			BufferedReader reader = new BufferedReader( new FileReader ("C:\\Users\\yeahmatte\\Documents\\git\\alpine\\AdventureHunter\\WebContent\\WEB-INF\\JSP\\Menu\\menu.html"));
			String         line = null;
			StringBuilder  stringBuilder = new StringBuilder();
			String         ls = System.getProperty("line.separator");

			while( ( line = reader.readLine() ) != null ) {
				stringBuilder.append( line );
				stringBuilder.append( ls );
			}
			return stringBuilder.toString();
		} catch (IOException e) {
			ErrorLogger.addLogEntry(e);
			return "NO SOUP FOR YOU";
		}
	}
	
	public static String getBodyStart(AbstractSessionUser sessionUser) {
		StringBuilder htmlBuilder = new StringBuilder();
		htmlBuilder.append("<table> \n <tr> \n <td colspan=\"2\"> \n" +
						   "<img src=\"/AdventureHunter/Img/Logo/AdventureHunterLogo_top.png\" alt=\"Adventure hunter logo\">");
		htmlBuilder.append("");
		htmlBuilder.append("</td> \n </tr> \n <tr> \n <td valing=\"top\"> \n <!-- Menu --> \n");
		if(!sessionUser.isGuest()) {
			htmlBuilder.append(getLeftMenu());
		} else {
			htmlBuilder.append("");
		}
		htmlBuilder.append("\n</td> \n <td valign=\"top\"> \n");
		return htmlBuilder.toString();
	}
	
	public static String getBodyEnd() {
		StringBuilder htmlBuilder = new StringBuilder();
		htmlBuilder.append("</td> \n </tr> \n </table> \n");
		return htmlBuilder.toString();
	}
}
