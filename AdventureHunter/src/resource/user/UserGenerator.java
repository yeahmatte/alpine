package resource.user;

import model.user.UserObjectGenerator;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.1
 * @version 0.4
 */
public class UserGenerator {
	private static UserGeneratorInterface generator;
	
	public static UserGeneratorInterface getInstance() {
		if(generator == null)
			generator = new UserObjectGenerator();
		return generator;
	}
}
