package resource.user;

import java.util.Calendar;

/**
 * The main representation of a user account in the system.
 * Is used for make changes to user accounts within the system.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public abstract class AbstractUserAccount extends AbstractUser{

	/**
	 * 
	 * @param userId
	 * @param userName
	 * @param userLevel
	 */
	protected AbstractUserAccount(Integer userId, String userName, Integer userLevel) {
		super(userId, userName, userLevel);
	}

	/**
	 * Returns a string that contains the first name, belonging to the given
	 * user account.
	 * 
	 * @return the first name
	 */
	public abstract String getFirstName();

	/**
	 * Returns a string that contains the surname, belonging to the given user
	 * account.
	 * 
	 * @return the surname
	 */
	public abstract String getSurname();

	/**
	 * Returns a string that contains the email, belonging to the given user
	 * account.
	 * 
	 * @return the email
	 */
	public abstract String getEmailAddress();
	
	/**
	 * Returns a string that contains the salt, belonging to the given user
	 * account.
	 * 
	 * @return the salt
	 */
	public abstract String getSalt();

	/**
	 * Returns a string that contains the password, belonging to the given user
	 * account.
	 * 
	 * @return the password
	 */
	public abstract String getPassword();

	/**
	 * Returns a boolean value that is either true or false, depending on if the
	 * user account is active or not.
	 * 
	 * @return true or false
	 */
	public abstract boolean isActive();

	/**
	 * Returns the date of the last login.
	 * 
	 * @return true or false
	 */
	public abstract Calendar getLastLogin();
	
	/**
	 * Returns the date when the user signed up for an account in the system.
	 * 
	 * @return true or false
	 */
	public abstract Calendar getSignUpDate();

	/**
	 * Sets the first name for the user for the given user account, to the name
	 * that is given as an argument.
	 * 
	 * @param name
	 *            that is the name that the current first name should be set to
	 */
	public abstract void setFirstName(String name);

	/**
	 * Sets the password for the user for the given user account, to the
	 * password that is given as an argument.
	 * 
	 * @param password
	 *            The new password in plain text
	 */
	public abstract void setPassword(String password);

	/**
	 * Sets the salt for the user for the given user account, to the
	 * password that is given as an argument.
     * @param salt
	 *            that is the password that the current password should be set
	 *            to
	 */
	public abstract void setSalt(String salt);
	
	/**
	 * Sets the surname for the user for the given user account, to the name
	 * that is given as an argument.
	 * 
	 * @param name
	 *            that is the name that the current surname should be set to
	 */
	public abstract void setSurname(String name);

	/**
	 * Sets the email for the user for the given user account, to the email that
	 * is given as an argument.
	 * 
	 * @param email
	 *            that is the email that the current email should be set to
	 */
	public abstract void setEmailAddress(String email);

	/**
	 * Sets the user level for a specific user account. 
	 *
	 * @param userLevel The new user level to set
	 */
	public abstract void setUserLevel(Integer userLevel);
	
	/**
	 *  Archives the user account
	 */
	public abstract void archive();
	
	/**
	 * Saves the user account in it's current state to the storage media.
	 * @return true if the account was saved otherwise false
	 */
	public abstract boolean saveUserAccount();
	
	/**
	 * Checks if password matches the accounts password.
	 * @param password The password to check in plain text
	 * @return True if its a match otherwise false
	 */
	public abstract boolean checkPassword(String password);
}