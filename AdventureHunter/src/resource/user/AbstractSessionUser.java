package resource.user;

import java.util.Calendar;

/**
 * Used to store information about a signed in user
 * between page requests.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public abstract class AbstractSessionUser extends AbstractUser{
	
	/**
	 * Constructor.
	 * @param userId User id
	 * @param userName User name
	 * @param userLevel User level
	 */
	protected AbstractSessionUser(Integer userId, String userName, Integer userLevel) {
		super(userId, userName, userLevel);
	}
	
	/**
	 * Returns a string that contains the first name, belonging to the given
	 * user account.
	 * 
	 * @return the first name
	 */
	public abstract String getFirstName();

	/**
	 * Returns the date of the last login.
	 * 
	 * @return Date when the user last logged in
	 */
	public abstract Calendar getLastLogin();

	/**
	 * Returns the date when the user signed up for an account in the system.
	 * 
	 * @return Date when the user signed up for an account in the system
	 */
	public abstract Calendar getSignUpDate();

	/**
	 * Returns the date when the user last made a get request.
	 * 
	 * @return Date of the last get request made by the user
	 */
	public abstract Calendar getLastPageRequest();
	
	/**
	 * Set the last page request of the account to the current time.
	 */
	public abstract void setLastPageRequest();
	
	/**
	 * Updates the database with the current LastLogin time.
	 */
	public abstract void saveLastLogin();
	
	/**
	 * 
	 * @return
	 */
	public abstract boolean isGuest();
}
