package resource.user;

/**
 * The super class for all user implementations.
 * Has three basic attributes that are present in all
 * user representation.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public abstract class AbstractUser {
	protected Integer userId;
	protected String userName;
	protected Integer userLevel;

	/**
	 * Default constructor 
	 * @param userId The userId of the user (See SRS for spec.)
	 * @param userName User name of the user (See SRS for spec.)
	 * @param userLevel User level of the user (See SRS for spec.)
	 */
	protected AbstractUser(Integer userId, String userName, Integer userLevel){
		this.userId = userId;
		this.userName = userName;
		this.userLevel = userLevel;
	}

	/**
	 * Returns an int that contains the user's Id, belonging to the given user
	 * account.
	 * 
	 * @return the User's Id
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * Returns a string that contains the username, belonging to the given user
	 * account.
	 * 
	 * @return the username
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Returns the current user level 
	 * 
	 * @return the user's access level within the system
	 */
	public Integer getUserLevel(){
		return userLevel;
	}
}
