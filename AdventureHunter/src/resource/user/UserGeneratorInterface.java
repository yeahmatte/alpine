package resource.user;

import java.util.List;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.2
 * @version 0.1
 */
public interface UserGeneratorInterface {

		/**
		 * Get a UserAccount from a userId.
		 * If the userId isnt found in the system a
		 * UserNotFoundException is thrown
		 * @param userId The userId to get the account for
		 * @return	Returns the UserAccount asscosiated with the userId
		 * @throws UserNotFoundException If the userId isnt found
		 */
		public AbstractUserAccount getUserAccount(int userId);
		
		/**
		 * Get a UserAccount from a userName.
		 * If the userId isnt found in the sytem a
		 * UserNotFoundException is thrown.
		 * @param userName The userName to get the account for
		 * @return	Returns the UserAccount asscosiated with the userName
		 * @throws UserNotFoundException If the userName isnt found
		 */
		public AbstractUserAccount getUserAccount(String userName);

		public AbstractDisplayUser getDisplayUser(AbstractUser userObject);

		public AbstractDisplayUser getDisplayUser(int userId);

		public List<AbstractDisplayUser> getDisplayUsers(List<Integer> userIds);
		
		/**
		 * Returns a list of display users ordered by user name.
		 * @param pageNo which page of 50 users to start at.
		 * @return A list that contains displayUsers.
		 */
		public List<AbstractDisplayUser> getUserList(int pageNo);

		/**
		 * Used to get a session user after a login attempt from a user.
		 * The method returns a AbstractSessionUser .
		 * @param userName Pattern validated User name from login
		 * @param password Pattern validated Password from login
		 * @return An SessionUser or if the user isn't found 
		 */
		public AbstractSessionUser getSessionUserFromLogin(String userName, String password) ;
		
		/**
		 * 
		 * @return
		 */
		public AbstractSessionUser getNewSessionGuest();
		
		/**
		 * Creates a new UserAccount in the system
		 * and returns a AbstractUserAccount with the information.
		 * This method will add a new random salt to the account and
		 * assign a userId.
		 * @param userName The username of the new account
		 * @param password The password of the new account
		 * @param firstName The first name of the new account
		 * @param surname The surname of the new account
		 * @param email The email addrss of the new account
		 * @return A AbstractUserAccount with the information
		 * @throws UserNotFoundException If the user could not be saved to the system
		 * @throws ObjectNotCreatedException If the user could not be saved to the system
		 */
		public AbstractUserAccount createNewUserAccount(String userName, String password, String firstName, String surname, String email);
		
		/**
		 * Checks if a userName is registered in the system
		 * @param userName The userName to check for
		 * @return True if the username if the userName isnt found otherwise false
		 */
		public boolean isUserNameAvailable(String userName);

}
