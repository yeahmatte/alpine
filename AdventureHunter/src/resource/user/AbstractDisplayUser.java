package resource.user;

import java.util.Calendar;

/**
 * Used to display user details of users other than the currently
 * signed in user. Has no set methods, for editing use AbstractUserAccount.
 * @author Mattias Mellhorn
 * @version 0.1
 * @since 0.1
 */
public abstract class AbstractDisplayUser extends AbstractUser {

	protected AbstractDisplayUser(int userId, String userName, int userLevel) {
		super(userId, userName, userLevel);
	}

	public AbstractDisplayUser() {
		super(null,null,null);
	}
	
	/**
	 * Returns a string that contains the first name, belonging to the given
	 * user account.
	 * 
	 * @return the first name
	 */
	public abstract String getFirstName();

	/**
	 * Returns a string that contains the surname, belonging to the given user
	 * account.
	 * 
	 * @return the surname
	 */
	public abstract String getSurname();

	/**
	 * Returns a string that contains the email, belonging to the given user
	 * account.
	 * 
	 * @return the email
	 */
	public abstract String getEmailAddress();

	/**
	 * Returns a boolean value that is either true or false, depending on if the
	 * user account is active or not.
	 * 
	 * @return true or false
	 */
	public abstract boolean isActive();

	/**
	 * Returns the date of the last login
	 * 
	 * @returnDate when the user last logged in
	 */
	public abstract Calendar getLastLogin();

	/**
	 * Returns the date when the user signed up for an account in the system
	 * 
	 * @return Date when the user signed up for an account in the system
	 */
	public abstract Calendar getSignUpDate();
}
