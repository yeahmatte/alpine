package resource.activity;

import java.util.Calendar;
import java.util.List;

import model.gpsanalyzer.AbstractGpsAnalyzerContainer;


/**
 * General contract for the activity implementation.
 * Defines the information the system want to get from the activities.
 * Allows for separation between the implementation of activity model
 * and the activity object the servlets uses.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public interface ActivityInterface {

	//General info
	public int getActivityId();
	
	public void setUserId(int userId);
	
	public Integer getUserId();
	
	public void setId(int activityId);
	
	public String getName();
	
	public String getActivityTypeName();
	
	public AbstractActivityType getActivityType();
	
	public String getDescription();
	
	public Calendar getStartTime();
	
	public double getStartLatitude();
	
	public double getStartLongitude();
	
	public List<GpsPointInterface> getGpsPoints();
	
	public AbstractGpsAnalyzerContainer getContainer(String containerName);
	
	public boolean hasContainer(String containerName);
	
	//Elevation
	public double getTotalElevationGained();
	
	public double getTotalElevationLost();
	
	public double getMinElevation();
	
	public double getMaxElevation();
	
	//Time
	public double getTotalElapsedTime();
	
	public double getTotalRestTime();
	
	public double getTotalActiveTime();
	
	public double getTotalTime();
	
	//Speed
	public double getAverageSpeed();
	
	public double getMaxSpeed();
	
	public double getMaxAcceleration();
	
	//Distance
	public double getTotalDistance();
	
	
	//Set values
	public void setName(String name);
	
	public void setDescription(String description);
	
	public void setActivityType(AbstractActivityType activityType);
	
	public void setStartTime(Calendar startTime);
	
	public void setStartCoords(double lat, double lon);
	
	public void setGpsPoints(List<GpsPointInterface> gpsPoints);
	
	public void setTotalDistance(double totalDistance);
	
	public void setTotalTime(double totalTime);
	
	//Save
	public void saveActivity();
	
}
