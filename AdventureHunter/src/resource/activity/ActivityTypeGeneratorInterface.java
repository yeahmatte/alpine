package resource.activity;

/**
 * General contract for the activity type generator implementation.
 * Defines the information the functions the generator must be
 * able to perform for the system.
 * Allows for separation between the implementation of activity type model
 * and the activity type objects the servlets uses.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public interface ActivityTypeGeneratorInterface {

	public AbstractActivityType getObjectFromString(String activityType);
}
