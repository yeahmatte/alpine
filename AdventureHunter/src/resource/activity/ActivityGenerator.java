package resource.activity;

import model.activity.ActivityObjectGenerator;

/**
 * Used to make the activity generator a singleton object.
 * This also allows the servlets to be unaware of the actual
 * implementation of the activity generator as long as it 
 * upholds the interface contract.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class ActivityGenerator {
	private static ActivityGeneratorInterface generator;
	
	public static ActivityGeneratorInterface getInstance() {
		if(generator == null)
			generator = new ActivityObjectGenerator();
		return generator;
	}
}
