package resource.activity;

import model.activity.ActivityTypeObjectGenerator;

/**
 * Used to make the activity type generator a singleton object.
 * This also allows the servlets to be unaware of the actual
 * implementation of the activity type generator as long as it 
 * upholds the interface contract.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public class ActivityTypeGenerator {
	private static ActivityTypeGeneratorInterface generator;
	
	public static ActivityTypeGeneratorInterface getInstance() {
		if(generator == null)
			generator = new ActivityTypeObjectGenerator();
		return generator;
	}
}
