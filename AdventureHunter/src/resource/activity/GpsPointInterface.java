package resource.activity;

import java.util.Calendar;

/**
 * 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public interface GpsPointInterface {
	
	//Value getters
	public double getLongitude();
	public double getLatitude();
	public double getElevation();
	public Calendar getTime();
	
	//Calculations
	public double elevationDifference(GpsPointInterface point);
	public double distanceToPoint2D(GpsPointInterface point);
	public double distanceToPoint3D(GpsPointInterface point);
	public double speedFromPoint(GpsPointInterface point);
	public double timeBetweenPoint(GpsPointInterface point);
}
