package resource.activity;

import java.util.ArrayList;
import java.util.List;

import model.gpsanalyzer.analyzerContainers.EmptyContainer;
import model.gpsanalyzer.AbstractGpsAnalyzerContainer;

/**
 * Contract for how to implement an activity type.
 * This should should be the only reference the servlets
 * have to the different activity types. 
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.2
 */
public abstract class AbstractActivityType {
	protected List<AbstractGpsAnalyzerContainer> containers = new ArrayList<AbstractGpsAnalyzerContainer>();
	protected String dbName;
	protected String typeName;
	
	public String toDbString() {
		// TODO Auto-generated method stub
		return dbName;
	}

	public String getTypeName() {
		return typeName;
	}
	public List<AbstractGpsAnalyzerContainer> getAnalyzerContainers() {
		return containers;
	}

	public AbstractGpsAnalyzerContainer getAnalyzerContainer(String containerName) {
		for(AbstractGpsAnalyzerContainer c : containers) {
			if(c.getContainerName().equals(containerName))
				return c;
		}
		return new EmptyContainer();
	}

	public boolean hasAnalyzerContainer(String containerName) {
		for(AbstractGpsAnalyzerContainer c : containers) {
			if(c.getContainerName().equals(containerName))
				return true;
		}
		return false;
	}
}
