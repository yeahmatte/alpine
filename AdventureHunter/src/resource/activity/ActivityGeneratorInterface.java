package resource.activity;

import java.io.InputStream;
import java.util.List;

import resource.user.AbstractUser;

/**
 * Contract for what the activity generator should implement.
 * Used to allow a abstract in the servlets.
 * @author Mattias Mellhorn
 * @since 0.3
 * @version 0.1
 */
public interface ActivityGeneratorInterface {
	
	public ActivityInterface createNewActivity(InputStream gpxFile, AbstractUser user);
	
	public ActivityInterface getActivity(int activityId, boolean getGpsPoints);
	
	public List<ActivityInterface> getUserLatestActivities(int userId);
}
